package Util;
import Logic.StatusLogic;
import Models.Card.Deck;
import java.util.ArrayList;

public class MapperStatus {
    private static ArrayList<Deck> tenBestDecks = new ArrayList<>();

    public MapperStatus() {
        tenBestDecks = StatusLogic.getTenBestDecksOfPlayer();
    }

    public static ArrayList<Deck> getTenBestDecks() {
        return tenBestDecks;
    }

    public static void setTenBestDecks(ArrayList<Deck> tenBestDecks) {
        MapperStatus.tenBestDecks = tenBestDecks;
    }
}
