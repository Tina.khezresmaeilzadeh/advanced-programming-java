public class Vector2D {

    private int x;
    private int y;

    public Vector2D() {
    }

    public Vector2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void add(Vector2D V){
        x += V.getX();
        y += V.getY();
    }

    public void addX(Vector2D v){
        this.x += x;
    }

    public void addY(Vector2D v){
        this.y += y;
    }
}
