package Interfaces;

import Logic.FinishStates;
import Util.Board;
import Util.PuzzlePiece;

public interface Swappable {
    static void swapPieces(int i, int j) {
        PuzzlePiece copy = Board.getPuzzlePieces().get(i).getClone();
        Board.getPuzzlePieces().get(i).setImage(Board.getPuzzlePieces().get(j).getImage());
        Board.getPuzzlePieces().get(i).setPieceNumber(Board.getPuzzlePieces().get(j).getPieceNumber());
        Board.getPuzzlePieces().get(j).setImage(copy.getImage());
        Board.getPuzzlePieces().get(j).setPieceNumber(copy.getPieceNumber());


        if (FinishStates.gameFinished()) {
            FinishStates.setGameState("finished");
        }
    }
}
