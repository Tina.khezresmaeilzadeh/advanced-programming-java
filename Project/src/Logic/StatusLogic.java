package Logic;
import Interfaces.DeckProducible;
import Models.Card.Deck;
import Models.Player;
import java.util.ArrayList;
import java.util.Collections;


public class StatusLogic  {


    public StatusLogic() {


    }



    public static ArrayList<Deck> getSortedAllDecks(ArrayList<Deck> allDecks) {
        Collections.sort(allDecks, Deck::compareTo);
        Collections.reverse(allDecks);
        return allDecks;
    }

    public static ArrayList<Deck> getTenBestDecksOfPlayer(){

        Player player = GameState.getInstance().getPlayer();
        ArrayList<Deck> sortedDecks = getSortedAllDecks(player.getAllDecks());
        ArrayList<Deck> tenBestDecks = new ArrayList<>();
        if(sortedDecks.size() >= 10){

            for(int i = 0 ; i < 10 ; i++ ){
                tenBestDecks.add(sortedDecks.get(i));
            }

        }
        else{
            for(int i = 0 ; i < sortedDecks.size() ; i++ ){
                tenBestDecks.add(sortedDecks.get(i));
            }
        }

        return tenBestDecks;

    }





}
