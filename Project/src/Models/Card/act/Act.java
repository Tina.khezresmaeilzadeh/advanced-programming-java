package Models.Card.act;

public class Act {

    private Effect effect;
    private EffectOccurringTime effectOccurringTime;
    private EffectOccurringPlace effectOccurringPlace;
    private boolean active = false;
    private int data = 0;
    private String description = "";


    public Act(Effect effect, EffectOccurringTime effectOccurringTime, EffectOccurringPlace effectOccurringPlace,
               boolean active, int data, String description) {
        this.effect = effect;
        this.effectOccurringTime = effectOccurringTime;
        this.effectOccurringPlace = effectOccurringPlace;
        this.active = active;
        this.data = data;
        this.description = description;
    }

    public Effect getEffect() {
        return effect;
    }

    public void setEffect(Effect effect) {
        this.effect = effect;
    }

    public EffectOccurringTime getEffectOccurringTime() {
        return effectOccurringTime;
    }

    public void setEffectOccurringTime(EffectOccurringTime effectOccurringTime) {
        this.effectOccurringTime = effectOccurringTime;
    }

    public EffectOccurringPlace getEffectOccurringPlace() {
        return effectOccurringPlace;
    }

    public void setEffectOccurringPlace(EffectOccurringPlace effectOccurringPlace) {
        this.effectOccurringPlace = effectOccurringPlace;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
