package Logic;

import Interfaces.CardProducible;
import Models.Card.Card;
import Models.Card.Deck;
import Models.Hero.*;
import Models.Player;
import Util.Log;
import Util.MapperPassive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class PlayLogic implements CardProducible {


    public static ArrayList<String> getRandomCards(Deck deck) {
        int totalItems = 3;
        Random rand = new Random();

        ArrayList<String> list = new ArrayList<>();

        for( String name : deck.getCards())
            list.add(name);

        // create a temporary list for storing
        // selected element
        ArrayList<String> newList = new ArrayList<>();
        for (int i = 0; i < totalItems; i++) {

            int randomIndex = rand.nextInt(list.size());

            // add element in temporary list
            newList.add(list.get(randomIndex));
            list.remove(list.get(randomIndex));
        }
        return newList;
    }



    private static Hero returnHero(String heroName){
        Hero currentHero;
        switch(heroName){
            case "Mage":
                currentHero = new Mage();
                break;
            case "Rouge":
                currentHero = new Rogue();
                break;
            case "Warlock":
                currentHero = new Warlock();
                break;
            case "Hunter":
                currentHero = new Hunter();
                break;
            case "Priest":
                currentHero = new Priest();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + heroName);
        }
        return currentHero;
    }


    public static int friendlyRemainedInDeck(){
        Player player = GameState.getInstance().getPlayer();
        return player.getCurrentDeck().getCards().size();
    }
    public static int enemyRemainedInDeck(){
        Player player = GameState.getInstance().getOpponentPlayer();
        return player.getCurrentDeck().getCards().size();
    }

    public static int friendlyRemainedInHand(){
        return GameState.getInstance().getPlayer().getCurrentHandCards().size();
    }

    public static int enemyRemainedInHand(){
        return GameState.getInstance().getOpponentPlayer().getCurrentHandCards().size();
    }

    public static void addToHandCard(){
        Player player = GameState.getInstance().getCurrentPlayer();
        Random rand = new Random();
        ArrayList<String> currentDeckCards = player.getCurrentDeck().getCards();
        if(currentDeckCards.size() != 0){
            if(GameState.getInstance().getCurrentPlayer().getCurrentHandCards().size() < 12){

                String selectedCard = currentDeckCards.get(rand.nextInt(currentDeckCards.size()));
                player.getCurrentDeck().getCards().remove(selectedCard);
                GameState.getInstance().getCurrentPlayer().getCurrentHandCards().add(selectedCard);

                try {
                    Log.writeBodyInLogFile("AddedToHandCard", selectedCard);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }else if(GameState.getInstance().getCurrentPlayer().getCurrentHandCards().size() == 12){

                String selectedCard = currentDeckCards.get(rand.nextInt(currentDeckCards.size()));
                player.getCurrentDeck().getCards().remove(selectedCard);

                try {
                    Log.writeBodyInLogFile("UnsuccessfulAddToHandCard",selectedCard);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }

    }


    public static void prepareForTheGame(){

        CollectionLogic.chooseCurrentDeck("Hunter Special", GameState.getInstance().getOpponentPlayer());

        // Friendly
        GameState.getInstance().getPlayer().setCurrentHandCards(getRandomCards(GameState.getInstance().getPlayer().getCurrentDeck()));
        System.out.println("Friendly random: " + GameState.getInstance().getPlayer().getCurrentHandCards());
        GameState.getInstance().setFriendlyHero(returnHero(GameState.getInstance().getPlayer().getCurrentHero()));
        // Enemy
        GameState.getInstance().getOpponentPlayer().setCurrentHandCards(getRandomCards(GameState.getInstance().getOpponentPlayer().getCurrentDeck()));
        System.out.println("Enemy random: " + GameState.getInstance().getOpponentPlayer().getCurrentHandCards());

        GameState.getInstance().setEnemyHero(returnHero(GameState.getInstance().getOpponentPlayer().getCurrentHero()));

        GameState.getInstance().getPlayer().setCurrentPassive(MapperPassive.getPassiveNameSelected());








    }

    public static void putCardOnGround(String cardNameSelected, Player player){

        if(player.getCurrentTurnResumedManas() - CardProducible.produceCardObject(cardNameSelected).getMana() >= 0){

            Card card = CardProducible.produceCardObject(cardNameSelected);

            player.getCurrentHandCards().remove(cardNameSelected);

            player.setCurrentTurnResumedManas(
                    player.getCurrentTurnResumedManas() - CardProducible.produceCardObject(cardNameSelected).getMana());

            if(card.getType().equals("Minion"))
                player.getCurrentOnGroundCards().add(cardNameSelected);
            System.out.println("Current Player on ground cards" + player.getCurrentOnGroundCards());

        }

    }
    public static void summonCard(int x, int y){

    }



    public static void endTheTurn(){



        // Mana Increasing
        if(GameState.getInstance().getCurrentPlayer().getCurrentTurnTotalManas() <10){
            GameState.getInstance().getCurrentPlayer().setCurrentTurnTotalManas(
                    GameState.getInstance().getCurrentPlayer().getCurrentTurnTotalManas() + 1);
            GameState.getInstance().getCurrentPlayer().setCurrentTurnResumedManas(
                    GameState.getInstance().getCurrentPlayer().getCurrentTurnTotalManas());
        }else if(GameState.getInstance().getCurrentPlayer().getCurrentTurnTotalManas() == 10){
            GameState.getInstance().getCurrentPlayer().setCurrentTurnTotalManas(10);
            GameState.getInstance().getCurrentPlayer().setCurrentTurnResumedManas(10);

        }

        // Change Turn
        GameState.getInstance().setFriendlyTurn(!GameState.getInstance().isFriendlyTurn());
        System.out.println(GameState.getInstance().isFriendlyTurn());
        if(GameState.getInstance().isFriendlyTurn())
            GameState.getInstance().setCurrentPlayer(GameState.getInstance().getPlayer());
        else
            GameState.getInstance().setCurrentPlayer(GameState.getInstance().getOpponentPlayer());


        //Add Card
        PlayLogic.addToHandCard();
    }
}
