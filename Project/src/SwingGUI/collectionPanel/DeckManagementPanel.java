package SwingGUI.collectionPanel;

import Logic.CollectionLogic;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import Util.Constants;
import Util.ImageLoading;
import Util.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class DeckManagementPanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;
    private Drawer drawer;
    private JTextField searchField;
    private static String newDeckName = "";
    private static String newHeroName = "";

    private static boolean renameState = false;
    private static boolean deleteDeckState = false;
    private static boolean changeHeroState = false;
    private static boolean chooseCurrentDeckState = false;
    private static boolean removeCardState = false;
    private static boolean addCardState = false;
    private static boolean okButtonClicked = false;

    private Rectangle okButton = new Rectangle(140, 50, 100, 40 );
    private Rectangle exit = new Rectangle(120, 670, 70, 40);
    private Rectangle menu = new Rectangle(20, 670, 70, 40);
    private Rectangle heroCards = new Rectangle(20, 520, 130, 40);
    private Rectangle deckCards = new Rectangle(170, 520, 120, 40);
    private Rectangle deleteDeck = new Rectangle(20, 320, 140, 40);
    private Rectangle rename = new Rectangle(20, 380, 140, 40);
    private Rectangle changeHero = new Rectangle(20, 440, 140, 40);
    private Rectangle addCard = new Rectangle(20, 150, 100, 40);
    private Rectangle removeCard = new Rectangle(130, 150, 120, 40);
    private Rectangle chooseCurrentDeck = new Rectangle(50, 570, 190, 50 );

    public DeckManagementPanel() {
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.FILTER_PANEL_BACKGROUND_IMAGE_ADDRESS);

        drawer = new Drawer();
        addMouseListener(this);

        searchField = new JTextField(15);
        searchField.setEnabled(true);
        add(searchField);
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.COLLECTION_FILTER_PANEL_WIDTH,
                Constants.COLLECTION_FILTER_PANEL_HEIGHT, null);

        drawer.drawMainCollectionPanelPart(g2d);
        drawer.drawDeckManagementPanel(g2d);
    }


    /*

    private Rectangle addCard = new Rectangle(20, 150, 100, 40);

     */


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        CollectionDeckPanel.setMessage("");


        int x = mouseEvent.getX();
        int y = mouseEvent.getY();
        if (menu.contains(x, y)) {

            renameState = false;
            deleteDeckState = false;
            changeHeroState = false;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;


            MainFrame.showMenuPanel();

        } else if (exit.contains(x, y)) {
            renameState = false;
            deleteDeckState = false;
            changeHeroState = false;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;

            Log.saveThePlayer();
            System.exit(0);



        } else if (okButton.contains(x, y)) {
            okButtonClicked = true;


            if (renameState) {
                newDeckName = searchField.getText();
                deleteDeckState = false;
                chooseCurrentDeckState = false;
                changeHeroState = false;
                removeCardState = false;
                addCardState = false;
                renameState = true;


            }else if(changeHeroState){
                newHeroName = searchField.getText();
                renameState = false;
                deleteDeckState = false;
                changeHeroState = true;
                chooseCurrentDeckState = false;
                removeCardState = false;
                addCardState = false;
            }


        } else if (chooseCurrentDeck.contains(x, y)) {

            CollectionDeckPanel.setMessage("Choose your desired deck.");
            renameState = false;
            deleteDeckState = false;
            changeHeroState = false;
            chooseCurrentDeckState = true;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;




        }else if(deleteDeck.contains(x, y)){

            renameState = false;
            deleteDeckState = true;
            changeHeroState = false;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;

            CollectionDeckPanel.setMessage("Please choose your desired deck to be deleted.");




        }else if(changeHero.contains(x, y)) {

            CollectionDeckPanel.setMessage("At first Please choose deck, then write the new hero in the text field and then click on ok.");
            renameState = false;
            deleteDeckState = false;
            changeHeroState = true;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;




        }else if(rename.contains(x, y)) {
            CollectionDeckPanel.setMessage("At first Please write the new deck name in the text field and then click on ok" +
                    "and then choose deck.");


            renameState = true;
            deleteDeckState = false;
            changeHeroState = false;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;





        }else if(removeCard.contains(x, y)) {


            if(CollectionCardManagement.isShowDeckCards()){
                renameState = false;
                deleteDeckState = false;
                changeHeroState = false;
                chooseCurrentDeckState = false;
                removeCardState = true;
                addCardState = false;
                okButtonClicked = false;


            }else
                CollectionDeckPanel.setMessage("At first please choose deck,then choose card.");



        }else if(addCard.contains(x, y)) {

            if(CollectionCardManagement.isShowDecks()){
                renameState = false;
                deleteDeckState = false;
                changeHeroState = false;
                chooseCurrentDeckState = false;
                removeCardState = false;
                addCardState = true;
                okButtonClicked = false;

            }





        }else if(heroCards.contains(x, y)){

            renameState = false;
            deleteDeckState = false;
            changeHeroState = false;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;


            MainFrame.showCollectionCardManagementPanel();
            CollectionCardManagement.setShowHeros(true);
            CollectionCardManagement.setShowHeroCards(false);
            CollectionCardManagement.setShowAllAvailableCards(false);
            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.heroCardPanel.repaint();
            CollectionDeckPanel.deckCardPanel.repaint();
            this.repaint();


        }else if(deckCards.contains(x, y)) {

            renameState = false;
            deleteDeckState = false;
            changeHeroState = false;
            chooseCurrentDeckState = false;
            removeCardState = false;
            addCardState = false;
            okButtonClicked = false;

            MainFrame.showCollectionDeckPanel();
            CollectionCardManagement.setShowHeros(true);
            CollectionCardManagement.setShowHeroCards(false);
            CollectionCardManagement.setShowAllAvailableCards(false);
            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.heroCardPanel.repaint();
            CollectionDeckPanel.deckCardPanel.repaint();
            this.repaint();
        }
        CollectionDeckPanel.deckCardPanel.repaint();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    public static boolean isRenameState() {
        return renameState;
    }

    public static void setRenameState(boolean renameState) {
        DeckManagementPanel.renameState = renameState;
    }

    public static boolean isDeleteDeckState() {
        return deleteDeckState;
    }

    public static void setDeleteDeckState(boolean deleteDeckState) {
        DeckManagementPanel.deleteDeckState = deleteDeckState;
    }

    public static boolean isChangeHeroState() {
        return changeHeroState;
    }

    public static void setChangeHeroState(boolean changeHeroState) {
        DeckManagementPanel.changeHeroState = changeHeroState;
    }

    public static boolean isChooseCurrentDeckState() {
        return chooseCurrentDeckState;
    }

    public static void setChooseCurrentDeckState(boolean chooseCurrentDeckState) {
        DeckManagementPanel.chooseCurrentDeckState = chooseCurrentDeckState;
    }

    public static boolean isRemoveCardState() {
        return removeCardState;
    }

    public static void setRemoveCardState(boolean removeCardState) {
        DeckManagementPanel.removeCardState = removeCardState;
    }

    public static String getNewDeckName() {
        return newDeckName;
    }

    public static void setNewDeckName(String newDeckName) {
        DeckManagementPanel.newDeckName = newDeckName;
    }

    public static String getNewHeroName() {
        return newHeroName;
    }

    public static void setNewHeroName(String newHeroName) {
        DeckManagementPanel.newHeroName = newHeroName;
    }

    public static boolean isAddCardState() {
        return addCardState;
    }

    public static void setAddCardState(boolean addCardState) {
        DeckManagementPanel.addCardState = addCardState;
    }

    public static boolean isOkButtonClicked() {
        return okButtonClicked;
    }

    public static void setOkButtonClicked(boolean okButtonClicked) {
        DeckManagementPanel.okButtonClicked = okButtonClicked;
    }
}
