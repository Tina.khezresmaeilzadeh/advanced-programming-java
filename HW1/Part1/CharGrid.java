import java.util.*;
import java.io.*;


public class CharGrid {
	private char[][] grid;

	/**
	 * Constructs a new CharGrid with the given grid.
	 * Does not make a copy.
	 * @param grid
	 */
	public CharGrid(char[][] grid) {
		this.grid = grid;
	}

	/**
	 * Returns the area for the given char in the grid.
	 * @param ch char
	 * @return area for given char
	 */
	public int charArea(char ch) {
		int check = 0;
		ArrayList<Integer> Index_of_rows = new ArrayList<Integer>();
		ArrayList<Integer> Index_of_columns = new ArrayList<Integer>();

		for (int i = 0 ; i< grid.length ; i++ )
		{
			for(int j = 0 ; j< grid[0].length ; j++ )
			{
				if (grid[i][j] == ch)
				{
					Index_of_rows.add(i);
					Index_of_columns.add(j);
					check++;


				}
			}
		}
		int Length = Collections.max(Index_of_rows) - Collections.min(Index_of_rows) + 1;
		int Width = Collections.max(Index_of_columns) - Collections.min(Index_of_columns) + 1;
		int Area = Length*Width;
		if (check == 0)
		{
			return 0;
		}
		else return Area;
	}

	/**
	 * Returns the count of '+' figures in the grid
	 * @return number of + in grid
	 */
	public int countPlus() {
		int Count = 0;
		for (int i = 1 ; i< grid.length - 1 ; i++ )
		{
			for(int j = 1 ; j< grid[0].length -1 ; j++ )
			{
				if (grid[i][j] == grid[i][j-1] && grid[i][j] == grid[i][j+1] &&  grid[i][j]== grid[i-1][j] && grid[i][j]== grid[i+1][j])
				{
					Count++;



				}
			}
		}

		return Count;
	}


}

