import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Window extends JFrame{
    private JLabel statusBar1;
    private JLabel statusBar2;
    private JLabel statusBar3;

    public Window() {


        statusBar1 = new JLabel("0");
        add(statusBar1, BorderLayout.SOUTH);


        statusBar2 = new JLabel("Number of removed lines : 0");
        add(statusBar2, BorderLayout.NORTH);

        statusBar3 = new JLabel();
        add(statusBar3, BorderLayout.CENTER);



        Canvas canvas = new Canvas(this);
        add(canvas);
        canvas.start();
        setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        setTitle("Tetris Game!");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);

    }

    public static void main(String[] args)  {
        //BufferedImage myImage = ImageIO.read(new File("./City Background.png"));
        Window window = new Window();
        window.setLocationRelativeTo(null);
        window.setVisible(true);


        try {
            playMusic();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }


    }

    public static void playMusic() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("./song.wav"));
        Clip clip = AudioSystem.getClip();
        clip.open(audioInputStream);
        clip.start();
        clip.loop(20);
    }




    public JLabel getStatusBar1() {
        return statusBar1;
    }
    public JLabel getStatusBar2() {
        return statusBar2;
    }
    public JLabel getStatusBar3() {
        return statusBar3;
    }



}