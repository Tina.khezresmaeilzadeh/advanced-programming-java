package Util;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageLoading {
    public BufferedImage loadImage(String imageAddress) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(imageAddress));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }



}
