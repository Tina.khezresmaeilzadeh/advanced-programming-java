package Util;
import java.util.ArrayList;

public class Board {

    private static ArrayList<PuzzlePiece> puzzlePieces = new ArrayList<>();
    private static int missingPiece = 0;


    public static ArrayList<PuzzlePiece> getPuzzlePieces() {
        return puzzlePieces;
    }

    public static void setPuzzlePieces(ArrayList<PuzzlePiece> puzzlePieces) {
        Board.puzzlePieces = puzzlePieces;
    }

    public static int getMissingPiece() {
        return missingPiece;
    }

    public static void setMissingPiece(int missingPiece) {
        Board.missingPiece = missingPiece;
    }
}
