package Models.Card;

import Interfaces.Visitable;
import Logic.PlayActions.DoPlay;

public class WeaponCard implements Visitable {
    private int durability;
    private int attack;

    public WeaponCard(int durability, int attack) {
        this.durability = durability;
        this.attack = attack;
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }


    @Override
    public void accept(DoPlay doPlay) {
        doPlay.play(this);
    }
}
