package Logic;
import Interfaces.DeckUpdatable;
import Models.Player;
import SwingGUI.MainFrame;
import Util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

public class LogInLogic  {
    private Player player;

    public void logInNewPlayer(String username, String password, JLabel errorPlace){

        player = new Player();
        player.setUsername(username);
        player.setPassword(password);
        errorPlace.setText("");
        if(username.length() == 0){
            errorPlace.setText("Please Enter Username !");

        }else if(usernameHasBeenUsed(username)) {

            errorPlace.setText("This Username Has Been Used !");

        }else if(password.length() == 0){

            errorPlace.setText("Please Enter Password !");

        }else{

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            try (FileWriter writer = new FileWriter("src/PlayersInformation/" +username+".json")) {
                gson.toJson(player, writer);
                // Save The Player
                GameState.getInstance().setPlayer(player);
                Log.writeTitleInLogFile();
                Log.writeBodyInLogFile("Sign_In", player.getUsername());
                // Go to Menu

                MainFrame.showMenuPanel();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    public void logInOldPlayer(String username, String password, JLabel errorPlace){

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        Reader reader = null;
        try {
            reader = new FileReader(

                    "src/PlayersInformation/" + username + ".json");

            // Convert JSON File to Java Object
            player = gson.fromJson(reader, Player.class);
            // Check The Correctness Of Password
            if (player.getPassword().equals(password)){
                // Save The Player
                GameState.getInstance().setPlayer(player);
                Log.writeBodyInLogFile("Sign_In", player.getUsername());
                // Go to Menu
                MainFrame.showMenuPanel();
            }else
                errorPlace.setText("Your Password is not Correct");
        } catch (FileNotFoundException e) {
            // When It does not find this username
            errorPlace.setText("There is not anyone with this username.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public boolean usernameHasBeenUsed(String Username){
        ArrayList<String> CurrentUsernames = new ArrayList<>();
        File folder = new File(
                "src/PlayersInformation");
        {
            File[] files = folder.listFiles();
            if (files == null){

                return false;
            }


            for (File file : files) {
                CurrentUsernames.add(file.getName());
                if(file.getName().equals(Username+".json")){

                    return true;
                }

            }

            return false;
        }


    }

}
