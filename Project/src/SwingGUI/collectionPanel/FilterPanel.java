package SwingGUI.collectionPanel;

import Logic.CollectionLogic;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import Util.Constants;
import Util.ImageLoading;
import Util.Log;
import Util.MapperCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;


public class FilterPanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;

    private boolean filteredShowingAvailableCards = false;
    private boolean filterShowingNotAvailableCards = false;
    private boolean filterByMana = false;
    private boolean filterBySearch = false;
    private int Mana = 0;

    private static String searchString = "";

    private Rectangle exit = new Rectangle(120, 670, 70, 40);
    private Rectangle menu = new Rectangle(20, 670, 70, 40);

    private Rectangle searchButton = new Rectangle(140, 50, 100, 40 );

    private Rectangle zeroMana = new Rectangle(20, 170, 20, 20);
    private Rectangle oneMana = new Rectangle(50, 170, 20, 20);
    private Rectangle twoMana = new Rectangle(80, 170, 20, 20);
    private Rectangle threeMana = new Rectangle(110, 170, 20, 20);
    private Rectangle fourMana = new Rectangle(140, 170, 20, 20);
    private Rectangle fiveMana = new Rectangle(170, 170, 20, 20);
    private Rectangle sixMana = new Rectangle(200, 170, 20, 20);
    private Rectangle sevenMana = new Rectangle(230, 170, 20, 20);
    private Rectangle eightMana = new Rectangle(20, 200, 20, 20);
    private Rectangle nineMana = new Rectangle(50, 200, 20, 20);
    private Rectangle tenMana = new Rectangle(80, 200, 30, 20);
    private Rectangle allMana = new Rectangle(120, 200, 40, 20);

    private Rectangle myCards = new Rectangle(20, 320, 120, 40);
    private Rectangle newCards = new Rectangle(20, 380, 120, 40);
    private Rectangle allCards = new Rectangle(20, 440, 120, 40);
    private Rectangle heroCards = new Rectangle(20, 520, 130, 40);
    private Rectangle deckCards = new Rectangle(170, 520, 120, 40);




    private Drawer drawer;

    private JTextField searchField;




    public FilterPanel() {
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.FILTER_PANEL_BACKGROUND_IMAGE_ADDRESS);

        drawer = new Drawer();
        addMouseListener(this);

        searchField = new JTextField(15);
        searchField.setEnabled(true);
        add(searchField);


    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.COLLECTION_FILTER_PANEL_WIDTH,
                Constants.COLLECTION_FILTER_PANEL_HEIGHT, null);


        drawer.drawMainCollectionPanelPart(g2d);
        drawer.drawFilterPanel(g2d);







    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int x = mouseEvent.getX();
        int y = mouseEvent.getY();


        if(menu.contains(x, y)){

            MainFrame.showMenuPanel();

        }else if(exit.contains(x, y)){

            Log.saveThePlayer();
            System.exit(0);

        }else if(heroCards.contains(x, y)){


            MainFrame.showCollectionCardManagementPanel();

            CollectionCardManagement.setShowHeros(true);
            CollectionCardManagement.setShowHeroCards(false);
            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.heroCardPanel.repaint();
            this.repaint();


        }else if(deckCards.contains(x, y)){

            MainFrame.showCollectionDeckPanel();
            CollectionCardManagement.setShowHeros(true);
            CollectionCardManagement.setShowHeroCards(false);
            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.heroCardPanel.repaint();
            this.repaint();

        }else if(searchButton.contains(x, y)){
                // Search
            searchString = searchField.getText();
            MapperCollection.filterSearch(searchString);
            this.repaint();

        }else if(myCards.contains(x, y)){

            filteredShowingAvailableCards = true;
            filterShowingNotAvailableCards = false;

                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
                CollectionCardManagement.heroCardPanel.repaint();

        }else if(newCards.contains(x, y)){

                filterShowingNotAvailableCards = true;
                filteredShowingAvailableCards = false;

                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
                CollectionCardManagement.heroCardPanel.repaint();

        }else if(allCards.contains(x, y)){
                filteredShowingAvailableCards = false;
                filterShowingNotAvailableCards = false;

                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
                CollectionCardManagement.heroCardPanel.repaint();

        }else if(zeroMana.contains(x, y)){
                filterByMana = true;
                Mana = 0;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(oneMana.contains(x, y)){
                filterByMana = true;
                Mana = 1;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(twoMana.contains(x, y)){
                filterByMana = true;
                Mana = 2;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(threeMana.contains(x, y)){
                filterByMana = true;
                Mana = 3;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(fourMana.contains(x, y)){
                filterByMana = true;
                Mana = 4;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(fiveMana.contains(x, y)){
                filterByMana = true;
                Mana = 5;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(sixMana.contains(x, y)){
                filterByMana = true;
                Mana = 6;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(sevenMana.contains(x, y)){
                filterByMana = true;
                Mana = 7;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(eightMana.contains(x, y)){
                filterByMana = true;
                Mana = 8;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(nineMana.contains(x, y)){
                filterByMana = true;
                Mana = 9;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(tenMana.contains(x, y)){
                filterByMana = true;
                Mana = 10;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);
        }else if(allMana.contains(x, y)){
                filterByMana = false;
                MapperCollection.filterOrUnfilterCards(filterByMana, filteredShowingAvailableCards, filterShowingNotAvailableCards, Mana);



        }
        CollectionCardManagement.heroCardPanel.repaint();







    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
