package Util;
import java.awt.*;
import java.util.ArrayList;

public class MapperPlay {

    private static ArrayList<String> currentFriendlyOnPlayGroundCards = new ArrayList<>();
    private static ArrayList<Rectangle> currentFriendlyOnPlayGroundRectangles = new ArrayList<>();
    private static ArrayList<String> currentFriendlyOnHandCards = new ArrayList<>();
    private static ArrayList<Rectangle> currentFriendlyOnHandRectangles = new ArrayList<>();

    private static ArrayList<String> currentEnemyOnPlayGroundCards = new ArrayList<>();
    private static ArrayList<Rectangle> currentEnemyOnPlayGroundRectangles = new ArrayList<>();
    private static ArrayList<String> currentEnemyOnHandCards = new ArrayList<>();
    private static ArrayList<Rectangle> currentEnemyOnHandRectangles = new ArrayList<>();

    public static ArrayList<String> getCurrentFriendlyOnPlayGroundCards() {
        return currentFriendlyOnPlayGroundCards;
    }

    public static void setCurrentFriendlyOnPlayGroundCards(ArrayList<String> currentFriendlyOnPlayGroundCards) {
        MapperPlay.currentFriendlyOnPlayGroundCards = currentFriendlyOnPlayGroundCards;
    }

    public static ArrayList<Rectangle> getCurrentFriendlyOnPlayGroundRectangles() {
        return currentFriendlyOnPlayGroundRectangles;
    }

    public static void setCurrentFriendlyOnPlayGroundRectangles(ArrayList<Rectangle> currentFriendlyOnPlayGroundRectangles) {
        MapperPlay.currentFriendlyOnPlayGroundRectangles = currentFriendlyOnPlayGroundRectangles;
    }

    public static ArrayList<String> getCurrentFriendlyOnHandCards() {
        return currentFriendlyOnHandCards;
    }

    public static void setCurrentFriendlyOnHandCards(ArrayList<String> currentFriendlyOnHandCards) {
        MapperPlay.currentFriendlyOnHandCards = currentFriendlyOnHandCards;
    }

    public static ArrayList<Rectangle> getCurrentFriendlyOnHandRectangles() {
        return currentFriendlyOnHandRectangles;
    }

    public static void setCurrentFriendlyOnHandRectangles(ArrayList<Rectangle> currentFriendlyOnHandRectangles) {
        MapperPlay.currentFriendlyOnHandRectangles = currentFriendlyOnHandRectangles;
    }

    public static ArrayList<String> getCurrentEnemyOnPlayGroundCards() {
        return currentEnemyOnPlayGroundCards;
    }

    public static void setCurrentEnemyOnPlayGroundCards(ArrayList<String> currentEnemyOnPlayGroundCards) {
        MapperPlay.currentEnemyOnPlayGroundCards = currentEnemyOnPlayGroundCards;
    }

    public static ArrayList<Rectangle> getCurrentEnemyOnPlayGroundRectangles() {
        return currentEnemyOnPlayGroundRectangles;
    }

    public static void setCurrentEnemyOnPlayGroundRectangles(ArrayList<Rectangle> currentEnemyOnPlayGroundRectangles) {
        MapperPlay.currentEnemyOnPlayGroundRectangles = currentEnemyOnPlayGroundRectangles;
    }

    public static ArrayList<String> getCurrentEnemyOnHandCards() {
        return currentEnemyOnHandCards;
    }

    public static void setCurrentEnemyOnHandCards(ArrayList<String> currentEnemyOnHandCards) {
        MapperPlay.currentEnemyOnHandCards = currentEnemyOnHandCards;
    }

    public static ArrayList<Rectangle> getCurrentEnemyOnHandRectangles() {
        return currentEnemyOnHandRectangles;
    }

    public static void setCurrentEnemyOnHandRectangles(ArrayList<Rectangle> currentEnemyOnHandRectangles) {
        MapperPlay.currentEnemyOnHandRectangles = currentEnemyOnHandRectangles;
    }
}
