﻿


### **Report Of HW_2 of Java**###
####**Tetris Game**####
####Tina KhezrEsmaeilzadeh####
####96101595####

> In this homework ,we worked on the famous *Tetris* game. I had some classes and functions explained in this report.

At first I made a Constants class for all constants of the game, including numbers and those 7 famous shapes for the game . Then , I decided to define those shapes as enum that each shape has 2 attributes: it's color than is random in each game and also it's coordinates that there are 4 critical points for the game.
Then I created window class that has all things that must be shown in the frame.It extends JFrame and has some statusbars and also music methods. the psvm method is here!
Then I created a Vector2D class( a great cheat from case study) for working easier with points ( points are used a lot in this project ! ).It has some getters and setters and also some  other things used.
After this we reach to the Canvas class( the brain of this project ). Everything happens here !
Before that, let me explain about Shape class. It holds 2 important methods for Rotating the shapes Clockwise and Anticlockwise .The reason I put them there will be explained soon.
The shape class has an enum type that tells the kind of the shape and the array of the shape. At first I wanted not to put the array of the shape but in rotating, I got the idea that changing the points of the coordinate matrix is easy (I'm talking about multiplying the point to the Rotation matrix with specified angle ),so I created a 2D array in the shape class.So , the reason of rotating the shapes in the shape class is that we change it's Arr2d in it's class by using a dot after the name of the shape.
Ok!
In the Canvas at first we implement actionperformed and then we use one rule:
Is it time to create a new shape or not ? if yes go and create it.else put the current shape one line downer.I wanted to use key Down in keyboard but in the order it has said that it should come down itself.
The idea of moving the shape is :
If it doesn't  go beyond the borders defined or doesn't hit another object ,move it and change the points place and return true.I returned a boolean because whenever it sreturns no it should continue it's process by actionperformed and if yes I should check that is everything finished ? Should I put new shape?
Then I used a  full line deleted method that it checks whether the lines are full or not. If they are, it deletes them,by shifting everything one line down.
In the new shape creator method, I did several things:
the first one is that:
for scores and max score in each stage ,I did some jobs like putting the scores of each stage in an array list and then find the maximum.I used statusbars in this game because they are cool. 
In keypressed it checks for pressing a key and then uses that check method for movement.At first I wanted to put 2 different methods , one for checking and the other for moving but it seemed not optimized. I also have an undo that it's information come from new shape creator method. There it saves the last scene of the Board and by pressing u , it recovers that again.
For the first time of course, it is different. The undo doesn't work in the first time.
My project has the ability of window resizing and I didn't want to distroy that.
So, I used statusbars instead of buttons and etc.







