package Models.Card.act;

public enum EffectOccurringTime {
    ThisTurn, NextTurn, Deathrattle, Battlecry, Trigger, Inspire, Echo
}
