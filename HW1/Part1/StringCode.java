import java.lang.Character;
import java.lang.Math;
import java.lang.String;
import java.lang.System;
import java.util.HashSet;
import java.util.Set;
import java.io.*;


public class StringCode {
	String str;

	StringCode(String c)
	{
		str = c;
	}



	/**
	 * Given a string, returns the length of the largest run.
	 * A a run is a series of adajcent chars that are the same.
	 * @param str
	 * @return The largest length of consecutive equal characters
	 */
	public static int maxCons(String str) {
		int counter = 1;
		int maximum = 1;
		for (int i = 0 ; i < str.length() -1  ; i++)
		{
			if (str.charAt(i) != str.charAt(i+1)) {
				maximum = Math.max(maximum,counter);
				counter = 1 ;

			}else if(str.charAt(i) == str.charAt(i+1))
			{
				counter++;

			}
		}
		maximum = Math.max(maximum,counter);
		return maximum;
	}


	/**
	 * Given a string, for each digit in the original string,
	 * replaces the digit with that many occurrences of the character
	 * following. So the string "a3tx2z" yields "attttxzzz".
	 * @param str
	 * @return blown up string
	 */
	public static String blowup(String str) {
		String Output_String = "";
		int i = 0;
		while( i < str.length() -1 )
		{
			if(Character.getNumericValue(str.charAt(i)) == 0 ){
				i = i+2;
				continue;
			}
			else if((Character.getNumericValue(str.charAt(i)) == 1 ) || (Character.getNumericValue(str.charAt(i)) == 2 ) || (Character.getNumericValue(str.charAt(i)) == 3) || (Character.getNumericValue(str.charAt(i)) == 4 ) ||
					(Character.getNumericValue(str.charAt(i)) == 5 ) || (Character.getNumericValue(str.charAt(i)) == 6 ) || (Character.getNumericValue(str.charAt(i)) == 7 ) || (Character.getNumericValue(str.charAt(i)) == 8 ) ||
					(Character.getNumericValue(str.charAt(i)) == 9 ) ){
				for (int j = 1 ; j < Character.getNumericValue(str.charAt(i))  ; j++) {
					Output_String = Output_String + str.charAt(i+1);
				}

				i++;
			}
			else{
				Output_String = Output_String + str.charAt(i);
				i++;
			}

		}
		Output_String = Output_String + str.charAt(str.length() -1);

		return Output_String;
	}

	/**
	 * Given 2 strings, consider all the substrings within them
	 * of length len. Returns true if there are any such substrings
	 * which appear in both strings.
	 */
	public static int Maximum_Giver(String a, String b)
	{
		if(a.length()==0 || b.length() == 0)
			return 0;
		if(a.charAt(a.length()-1) == b.charAt(b.length()-1))

			return 1 + Maximum_Giver(a.substring(0,a.length()-1), b.substring(0,b.length()-1));
		else
			return Math.max(Maximum_Giver(a,b.substring(0,b.length()-1)),Maximum_Giver(b,a.substring(0,a.length()-1)));

	}
	public static boolean isIntersect(String a, String b, int len)
	{
		if (Maximum_Giver(a,b)>=len)
		{
			return true;
		}else
		{
			return false;
		}
	}


}

