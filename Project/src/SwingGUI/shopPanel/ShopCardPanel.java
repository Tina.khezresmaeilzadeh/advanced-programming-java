package SwingGUI.shopPanel;
import Logic.CollectionLogic;
import Logic.ShopLogic;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import SwingGUI.collectionPanel.CollectionCardManagement;
import Util.Constants;
import Util.ImageLoading;
import Util.MapperShop;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class ShopCardPanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;
    private Drawer drawer;
    String message = "";

    public ShopCardPanel() {
        addMouseListener(this);
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.SHOP_PANEL_BACKGROUND_IMAGE_ADDRESS);
        drawer = new Drawer();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;


        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.COLLECTION_CARD_PANEL_WIDTH,
                Constants.COLLECTION_CARD_PANEL_HEIGHT, null);


        // Message
        g2d.setColor(Color.RED);
        g2d.drawString(message, 800, 400);

        ShopPanel.setCardsNamesAndPositions(drawer.drawCards(g2d, MapperShop.getCurrentCards(), false));

    }

        @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int x = mouseEvent.getX();
        int y = mouseEvent.getY();

        if(ShopPanel.isBuyState()){



            ShopPanel.getCardsNamesAndPositions().forEach((k, v) -> {
                if( v.contains(x, y)){
                    System.out.println(k);
                    message = ShopLogic.buyCard(k);
                    MapperShop.produceCurrentCards(ShopPanel.isBuyState(), ShopPanel.isSellState());
                    ShopPanel.shopCardPanel.repaint();

                }
            });

        }else if(ShopPanel.isSellState()){



            message = "";
            ShopPanel.getCardsNamesAndPositions().forEach((k, v) -> {
                if( v.contains(x, y)){
                    ShopLogic.sellCard(k);
                    MapperShop.produceCurrentCards(ShopPanel.isBuyState(), ShopPanel.isSellState());
                    ShopPanel.shopCardPanel.repaint();

                }
            });

        }

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
