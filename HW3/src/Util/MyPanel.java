package Util;

import Logic.FinishStates;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MyPanel extends JPanel {


    public MyPanel() {

    }

    public int getPanelHeight(){
        return this.getHeight();
    }

    public int getPanelWidth(){
        return this.getWidth();
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (PuzzlePiece piece : Board.getPuzzlePieces()) {
            g.drawImage(piece.getImage(), piece.location.getX(), piece.location.getY(),
                    (int) this.getSize().getWidth() / 3, (int) this.getSize().getHeight() / 3, null);
        }
    }
}
