package SwingGUI.collectionPanel;
import SwingGUI.Drawer;
import Util.Constants;
import Util.ImageLoading;
import Util.MapperCollection;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

public class CollectionCardManagement extends JPanel  {

    // State


    private static boolean showHeros = true;
    private static boolean showHeroCards = false;

    private static boolean showDecks = true;
    private static boolean showDeckCards = false;

    private static boolean showAllAvailableCards = false;

    private BufferedImage backgroundImage;
    private Drawer drawer ;
    private static MapperCollection mapperCollection ;

    public static HeroCardPanel heroCardPanel;
    public static FilterPanel filterPanel ;


    private static HashMap<String, Rectangle> heroNamesAndPositions = new HashMap<>();
    private static HashMap<String, Rectangle> cardNamesAndPositions = new HashMap<>();
    private static HashMap<String, Rectangle> deckNamesAndPositions = new HashMap<>();
    private static HashMap<String, Rectangle> deckCardNamesAndPositions = new HashMap<>();


    private static String heroNameClicked = "";
    private static String cardNameClicked = "";

    public static boolean isShowAllAvailableCards() {
        return showAllAvailableCards;
    }

    public static void setShowAllAvailableCards(boolean showAllAvailableCards) {
        CollectionCardManagement.showAllAvailableCards = showAllAvailableCards;
    }

    public static HashMap<String, Rectangle> getDeckCardNamesAndPositions() {
        return deckCardNamesAndPositions;
    }

    public static void setDeckCardNamesAndPositions(HashMap<String, Rectangle> deckCardNamesAndPositions) {
        CollectionCardManagement.deckCardNamesAndPositions = deckCardNamesAndPositions;
    }

    public static String getCardNameClicked() {
        return cardNameClicked;
    }

    public static void setCardNameClicked(String cardNameClicked) {
        CollectionCardManagement.cardNameClicked = cardNameClicked;
    }

    public static HashMap<String, Rectangle> getHeroNamesAndPositions() {
        return heroNamesAndPositions;
    }

    public static void setHeroNamesAndPositions(HashMap<String, Rectangle> heroNamesAndPositions) {
        CollectionCardManagement.heroNamesAndPositions = heroNamesAndPositions;
    }

    public static HashMap<String, Rectangle> getCardNamesAndPositions() {
        return cardNamesAndPositions;
    }

    public static void setCardNamesAndPositions(HashMap<String, Rectangle> cardNamesAndPositions) {
        CollectionCardManagement.cardNamesAndPositions = cardNamesAndPositions;
    }

    public static String getHeroNameClicked() {
        return heroNameClicked;
    }

    public static void setHeroNameClicked(String heroNameClicked) {
        CollectionCardManagement.heroNameClicked = heroNameClicked;
    }


    public static boolean isShowHeros() {
        return showHeros;
    }

    public static void setShowHeros(boolean showHeros) {
        CollectionCardManagement.showHeros = showHeros;
    }

    public static boolean isShowHeroCards() {
        return showHeroCards;
    }

    public static void setShowHeroCards(boolean showHeroCards) {
        CollectionCardManagement.showHeroCards = showHeroCards;
    }

    public static boolean isShowDecks() {
        return showDecks;
    }

    public static void setShowDecks(boolean showDecks) {
        CollectionCardManagement.showDecks = showDecks;
    }

    public static boolean isShowDeckCards() {
        return showDeckCards;
    }

    public static void setShowDeckCards(boolean showDeckCards) {
        CollectionCardManagement.showDeckCards = showDeckCards;
    }

    public static MapperCollection getMapperCollection() {
        return mapperCollection;
    }

    public static void setMapperCollection(MapperCollection mapperCollection) {
        CollectionCardManagement.mapperCollection = mapperCollection;
    }

    public static HashMap<String, Rectangle> getDeckNamesAndPositions() {
        return deckNamesAndPositions;
    }

    public static void setDeckNamesAndPositions(HashMap<String, Rectangle> deckNamesAndPositions) {
        CollectionCardManagement.deckNamesAndPositions = deckNamesAndPositions;
    }

    public CollectionCardManagement() {

        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.FILTER_PANEL_BACKGROUND_IMAGE_ADDRESS);

        drawer = new Drawer();
        mapperCollection = new MapperCollection();


        BorderLayout layout = new BorderLayout();
        setLayout(layout);





        heroCardPanel = new HeroCardPanel();
        heroCardPanel.setPreferredSize(
                new Dimension(Constants.COLLECTION_CARD_PANEL_WIDTH, Constants.COLLECTION_CARD_PANEL_HEIGHT));

        heroCardPanel.setBorder(new LineBorder(Color.BLACK));
        JScrollPane scrollPane1 = new JScrollPane(heroCardPanel);
        scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        scrollPane1.setPreferredSize(new Dimension(1000, 500));
        scrollPane1.setLocation(200, 200);
        scrollPane1.setLayout(new ScrollPaneLayout());
        scrollPane1.setEnabled(true);
        add(scrollPane1, BorderLayout.EAST);


        filterPanel = new FilterPanel();
        filterPanel.setPreferredSize(new Dimension(300, 800));
        filterPanel.setBorder(new LineBorder(Color.BLACK));
        filterPanel.setEnabled(true);
        add(filterPanel, BorderLayout.WEST);

    }



    @Override
    protected void paintComponent(Graphics g){

        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.FRAME_WIDTH,
                Constants.FRAME_HEIGHT, null);




    }





}
