package Interfaces;
import Models.Card.Deck;

public interface DeckUpdatable {

    static void updateDeck(Deck deck){
        if(deck.getNumOfPlays() == 0)
            deck.setWinsToPlays(0);
        else
            deck.setWinsToPlays(deck.getNumOfWins()/deck.getNumOfPlays());


    }
}
