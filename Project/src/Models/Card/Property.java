package Models.Card;

import Models.Card.act.Act;

public class Property {

    private boolean acted = false;
    private Act act;
    private boolean battlecry = false;
    private boolean deathrattle = false;
    private boolean discover = false;
    private boolean rush = false;
    private boolean charge = false;
    private boolean divineShield = false;
    private boolean taunt = false;
    private boolean lifeSteal = false;
    private boolean poisonous = false;
    private boolean overkill = false;
    private boolean twinspell = false;
    private boolean reborn = false;
    private boolean outcast = false;
    private boolean dormant = false;
    private boolean inspire = false;
    private boolean windfury = false;
    private boolean megaWindfury = false;
    private boolean restore = false;
    private boolean echo = false;
    private boolean stealth = false;

    public boolean isActed() {
        return acted;
    }

    public void setActed(boolean acted) {
        this.acted = acted;
    }

    public Act getAct() {
        return act;
    }

    public void setAct(Act act) {
        this.act = act;
    }

    public boolean isBattlecry() {
        return battlecry;
    }

    public void setBattlecry(boolean battlecry) {
        this.battlecry = battlecry;
    }

    public boolean isDeathrattle() {
        return deathrattle;
    }

    public void setDeathrattle(boolean deathrattle) {
        this.deathrattle = deathrattle;
    }

    public boolean isDiscover() {
        return discover;
    }

    public void setDiscover(boolean discover) {
        this.discover = discover;
    }

    public boolean isRush() {
        return rush;
    }

    public void setRush(boolean rush) {
        this.rush = rush;
    }

    public boolean isCharge() {
        return charge;
    }

    public void setCharge(boolean charge) {
        this.charge = charge;
    }

    public boolean isDivineShield() {
        return divineShield;
    }

    public void setDivineShield(boolean divineShield) {
        this.divineShield = divineShield;
    }

    public boolean isTaunt() {
        return taunt;
    }

    public void setTaunt(boolean taunt) {
        this.taunt = taunt;
    }

    public boolean isLifeSteal() {
        return lifeSteal;
    }

    public void setLifeSteal(boolean lifeSteal) {
        this.lifeSteal = lifeSteal;
    }

    public boolean isPoisonous() {
        return poisonous;
    }

    public void setPoisonous(boolean poisonous) {
        this.poisonous = poisonous;
    }

    public boolean isOverkill() {
        return overkill;
    }

    public void setOverkill(boolean overkill) {
        this.overkill = overkill;
    }

    public boolean isTwinspell() {
        return twinspell;
    }

    public void setTwinspell(boolean twinspell) {
        this.twinspell = twinspell;
    }

    public boolean isReborn() {
        return reborn;
    }

    public void setReborn(boolean reborn) {
        this.reborn = reborn;
    }

    public boolean isOutcast() {
        return outcast;
    }

    public void setOutcast(boolean outcast) {
        this.outcast = outcast;
    }

    public boolean isDormant() {
        return dormant;
    }

    public void setDormant(boolean dormant) {
        this.dormant = dormant;
    }

    public boolean isInspire() {
        return inspire;
    }

    public void setInspire(boolean inspire) {
        this.inspire = inspire;
    }

    public boolean isWindfury() {
        return windfury;
    }

    public void setWindfury(boolean windfury) {
        this.windfury = windfury;
    }

    public boolean isMegaWindfury() {
        return megaWindfury;
    }

    public void setMegaWindfury(boolean megaWindfury) {
        this.megaWindfury = megaWindfury;
    }

    public boolean isRestore() {
        return restore;
    }

    public void setRestore(boolean restore) {
        this.restore = restore;
    }

    public boolean isEcho() {
        return echo;
    }

    public void setEcho(boolean echo) {
        this.echo = echo;
    }

    public boolean isStealth() {
        return stealth;
    }

    public void setStealth(boolean stealth) {
        this.stealth = stealth;
    }
}
