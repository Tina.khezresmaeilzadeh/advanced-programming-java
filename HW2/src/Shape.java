import java.awt.*;
import java.util.Random;

public class Shape {
    private Constants.DefaultShapes pieceShape;
    private int[][] Arr2d;

    public Shape() {
        Arr2d = new int[4][2];
        setShape(Constants.DefaultShapes.BiShekl);
    }

    public void setShape(Constants.DefaultShapes shape) {
        for (int i = 0; i < 4; i++) {

            for (int j = 0; j < 2; ++j) {

                Arr2d[i][j] = shape.getCoordinates()[i][j];

            }
        }

        pieceShape = shape;
    }

    public Constants.DefaultShapes getPieceShape() {
        return pieceShape;
    }

    public void setPieceShape(Constants.DefaultShapes pieceShape) {
        this.pieceShape = pieceShape;
    }

    public int[][] getArr2d() {
        return Arr2d;
    }

    public void setArr2d(int[][] arr2d) {
        Arr2d = arr2d;
    }



    public Constants.DefaultShapes getShape() {
        return pieceShape;
    }

    public void setRandomShape() {
        Random r = new Random();
        int x = Math.abs(r.nextInt()) % 7 + 1;
        Constants.DefaultShapes[] values = Constants.DefaultShapes.values();
        setShape(values[x]);

    }

    public int minY() {
        int m = Arr2d[0][1];

        for (int i = 0; i < 4; i++) {
            m = Math.min(m, Arr2d[i][1]);
        }

        return m;
    }


    public Shape rotateAntiClockwise() {
        if (pieceShape == Constants.DefaultShapes.window)
            return this;

        Shape output = new Shape();
        output.pieceShape = pieceShape;

        for (int i = 0; i < 4; i++) {

            output.Arr2d[i][0] = Arr2d[i][1];
            output.Arr2d[i][1] = -Arr2d[i][0];

        }

        return output;
    }

    public Shape rotateClockwise() {
        if (pieceShape == Constants.DefaultShapes.window)
            return this;

        Shape output = new Shape();
        output.pieceShape = pieceShape;

        for (int i = 0; i < 4; i++) {

            output.Arr2d[i][0] = -Arr2d[i][1];
            output.Arr2d[i][1] = Arr2d[i][0];


        }

        return output;
    }





}
