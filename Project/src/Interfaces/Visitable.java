package Interfaces;

import Logic.PlayActions.DoPlay;

public interface Visitable {

    public void accept(DoPlay doPlay);
}
