package SwingGUI.collectionPanel;

import Logic.CollectionLogic;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import Util.Constants;
import Util.ImageLoading;
import Util.MapperCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class DeckCardPanel extends JPanel implements MouseListener {
    private BufferedImage backgroundImage;
    private Drawer drawer;
    private static String selectedDeckName;

    public static String getSelectedDeckName() {
        return selectedDeckName;
    }

    public static void setSelectedDeckName(String selectedDeckName) {
        DeckCardPanel.selectedDeckName = selectedDeckName;
    }

    public DeckCardPanel() {
        addMouseListener(this);
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.CardPANEL_BACKGROUND_IMAGE_ADDRESS);
        drawer = new Drawer();

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;




        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.COLLECTION_CARD_PANEL_WIDTH,
                Constants.COLLECTION_CARD_PANEL_HEIGHT, null);

        if(CollectionCardManagement.isShowDecks()){

            CollectionCardManagement.setDeckNamesAndPositions(drawer.drawAllDecks(g2d));


        }else if(CollectionCardManagement.isShowDeckCards()){
            CollectionCardManagement.setDeckCardNamesAndPositions(
                    drawer.drawCards(g2d, CollectionCardManagement.getMapperCollection().getCurrentDeckCards(), false));


        }else if(CollectionCardManagement.isShowAllAvailableCards()){
            CollectionCardManagement.setDeckCardNamesAndPositions(
                    drawer.drawCards(g2d, CollectionCardManagement.getMapperCollection().getAllAvailableCards(), false));
        }


        g2d.setFont(new Font("Courier New", Font.BOLD, 40));
        g2d.setColor(Color.RED);
        g2d.drawString(CollectionDeckPanel.getMessage(), 20, 250);





        }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        CollectionDeckPanel.setMessage("");
        this.repaint();


        int x = mouseEvent.getX();
        int y = mouseEvent.getY();

        if(CollectionCardManagement.isShowDecks()){

            CollectionCardManagement.getDeckNamesAndPositions().forEach((k, v) -> {
                if( v.contains(x, y)){


                    selectedDeckName = k;
                    MapperCollection.doCorrectActionForClickingOnDeck(k,
                            DeckManagementPanel.getNewDeckName(), DeckManagementPanel.getNewHeroName());
                    CollectionDeckPanel.deckCardPanel.repaint();

                }
            });

        }else if(CollectionCardManagement.isShowDeckCards()) {

            for(Rectangle rectangle: MapperCollection.getCurrentDrawnRectangles()){

                if(rectangle.contains(x, y)){

                    int index = MapperCollection.getCurrentDrawnRectangles().indexOf(rectangle);
                    String cardNameSelected = MapperCollection.getCurrentDrawnCards().get(index);

                    CollectionCardManagement.setCardNameClicked(cardNameSelected);
                    MapperCollection.removeCard(cardNameSelected, selectedDeckName);
                    CollectionDeckPanel.deckCardPanel.repaint();

                }
            }



        }

        CollectionDeckPanel.deckCardPanel.repaint();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
