package Util;

import java.util.ArrayList;

public class Constants {
    public static final int FRAME_WIDTH = 1400;
    public static final int FRAME_HEIGHT = 800;


    public static final int COLLECTION_FILTER_PANEL_WIDTH = 300;
    public static final int COLLECTION_FILTER_PANEL_HEIGHT = 800;

    public static final int COLLECTION_CARD_PANEL_WIDTH = 1000;
    public static final int COLLECTION_CARD_PANEL_HEIGHT = 4500;

    public static final int STATUS_CARD_PANEL_WIDTH = 1400;
    public static final int STATUS_CARD_PANEL_HEIGHT = 1300;





    public static final String PLAYGROUND_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_PlayGround.jpg";
    public static final String MENU_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_Menu.jpg";
    public static final String LOGIN_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_Login.png";
    public static final String STATUS_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_Status.jpg";
    public static final String COLLECTION_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_Collection.jpg";
    public static final String SHOP_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_Shop.jpg";
    public static final String HERO_IMAGES_ADDRESSES = "src/Photos/HeroImages";
    public static final String CARD_IMAGES_ADDRESSES = "src/Photos/Cards/New folder";
    public static final String FILTER_PANEL_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_FilterPanel.jpg";
    public static final String CardPANEL_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_CardPanel.jpg";
    public static final String SHOP_MANAGEMENT_PANEL_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_ShopManagementPanel.jpg";
    public static final String SHOP_PANEL_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_ShopPanel.jpg";
    public static final String SHOP_CARD_PANEL_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Background_ShopCardPanel.png";
    public static final String CARD_LAYOUT_ADDRESS = "src/Photos/CardLayout.png";
    public static final String WEAPON_CARD_LAYOUT_ADDRESS = "src/Photos/WeaponCardLayout.png";
    public static final String STATUS_CARD_PANEL_BACKGROUND_IMAGE_ADDRESS = "src/Photos/Forest.jpg";







}
