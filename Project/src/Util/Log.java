package Util;

import Logic.GameState;
import Logic.PlayLogic;
import Models.Player;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class Log {


    public static String currentTimestamp() {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        DateFormat f = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        return f.format(c.getTime());
    }





    public static void writeBodyInLogFile(String title, String description) throws IOException {

        Player player = GameState.getInstance().getPlayer();
        FileWriter file = new FileWriter("src/LogFiles/" +
                player.getUsername() + "-" + player.getPassword() + ".log", true);
        file.write(title + " " + currentTimestamp() + " " + description);
        file.write("\n");
        file.close();

    }
    public static void writeErrorInLogFile(String title, String description) throws IOException {
        Player player = GameState.getInstance().getPlayer();
        FileWriter file = new FileWriter("src/LogFiles/" + player.getUsername() +
                "-" + player.getPassword() + ".log", true);
        file.write(title + " " + currentTimestamp() + " " + description);
        file.write("\n");
        file.close();

    }

    public static void writeTitleInLogFile() throws IOException {

        Player player = GameState.getInstance().getPlayer();
        FileWriter file = new FileWriter("src/LogFiles/" + player.getUsername() +
                "-" + player.getPassword() + ".log", true);
        file.write("USER:" + player.getUsername() + "\n" + "Created At " + currentTimestamp() + "\n"+ "PASSWORD : "
                + player.getPassword());
        file.write("\n\r");
        file.close();

    }

    public static void deletedAtAdder() throws IOException {
        Player player = GameState.getInstance().getPlayer();
        FileWriter out = new FileWriter("src/LogFiles/Temp.log", true);
        File file = new File("src/LogFiles/" + player.getUsername() + "-" + player.getPassword() + ".log");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine() )!= null){
            System.out.println(line);
            out.write(line);
            out.write("\n");
            //if(line == ""){
                //out.write("Deleted At :" + currentTimestamp() );
                //out.write("\n");
            //}
        }

    }
    public static void writeDeletedAt() {
        Player player = GameState.getInstance().getPlayer();
        File file = new File("src/LogFiles/"+player.getUsername()+"-"+ player.getPassword()+ ".log");
        FileReader fr = null;
        try {
            fr = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader br = new BufferedReader(fr);
        StringBuffer sb = new StringBuffer();
        String line = "";
        while(true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (line.equals("")){
                sb.append("Deleted At: " + currentTimestamp() + "\n");
            }
            sb.append(line);
            sb.append("\n");
        }
        try {
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new FileWriter("src/LogFiles/"+
                    player.getUsername()+"-"+ player.getPassword()+ ".log"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            output.write(sb.toString());
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void deleteAndRename() throws IOException {
        Player player = GameState.getInstance().getPlayer();

        File dir = new File("src/LogFiles");

        String source = dir.getCanonicalPath() + File.separator + player.getUsername()+"-"+ player.getPassword()+ ".log";
        String dest = dir.getCanonicalPath() + File.separator +
                player.getUsername()+"-"+ player.getPassword()+"deleted" + ".log";

        File file1 = new File(source);
        File file2 = new File(dest);
        boolean b2 = file2.delete();
        System.out.println(b2);
    }

    public static void saveThePlayer(){
        Player player = GameState.getInstance().getPlayer();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter(
                "src/PlayersInformation/"+ player.getUsername() +".json")) {
            gson.toJson(player, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }





}
