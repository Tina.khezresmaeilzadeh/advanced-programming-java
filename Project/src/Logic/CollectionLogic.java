package Logic;
import Interfaces.CardProducible;
import Models.Card.Card;
import Models.Card.Deck;
import Models.Hero.*;
import Models.Player;
import Util.Log;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

public class CollectionLogic implements CardProducible {





    /*
    public static ArrayList<String> readTotalHeroNames() {
        Gson gson = new Gson();
        Reader reader = null;
        try {
            reader = new FileReader(
                    "src/GameGeneralInformation/totalHeroNames.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return gson.fromJson(reader, ArrayList.class);
    }

     */



    public static ArrayList<String> readTotalClassNames() {
        Gson gson = new Gson();
        Reader reader = null;
        try {
            reader = new FileReader(
                    "src/GameGeneralInformation/totalClassNames.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return gson.fromJson(reader, ArrayList.class);
    }


    public static ArrayList<String> readTotalCardNames(){

        Gson gson = new Gson();
        Reader reader = null;
        try {
            reader = new FileReader(
                    "src/GameGeneralInformation/totalGameCards.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return gson.fromJson(reader, ArrayList.class);
    }

    // Classification Of Cards
    public static ArrayList<String> giveHeroCards(String classOfCard){
        ArrayList<String> heroCards = new ArrayList<>();

        ArrayList<Card> totalCards = new ArrayList<>();
        for(String cardName : readTotalCardNames()){
            totalCards.add(CardProducible.produceCardObject(cardName));
        }



        for(Card card : totalCards){

            if(card.getClassOfCard().equals(classOfCard)){
                heroCards.add(card.getName());

            }

        }
        return heroCards;
    }

    // Distinguishing Cards
    public static boolean isCardAvailable( String cardName){

        Player player = GameState.getInstance().getPlayer();

        if(player.getAvailableCards().contains(cardName)){
            return true;
        }
        return false;

    }


    // Decks Shown
    public static HashMap<String, String> getDecksAndHeros(){

        Player player = GameState.getInstance().getPlayer();

        HashMap<String, String> decksAndHerosMap = new HashMap<>();

        for(Deck deck : player.getAllDecks()){
            decksAndHerosMap.put(deck.getName(), deck.getHeroName());
        }
        return decksAndHerosMap;

    }


    public static void changeDeckName(String oldDeckName, String newDeckName){
        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(oldDeckName))
                deck.setName(newDeckName);
        }
    }


    public boolean changeDeckHero(String deckName, String newHeroName){
        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(deckName)){
                if(deck.getHeroName().equals("Neutral")){
                    deck.setHeroName(newHeroName);
                    return true;
                }else{

                    for(String cardName : deck.getCards()){
                        Card card = CardProducible.produceCardObject(cardName);
                        if( card.getClassOfCard().equals(deck.getHeroName())){
                            return false;
                        }else{
                            deck.setHeroName(newHeroName);
                            return true;
                        }

                    }

                }

            }

        }
        return false;
    }

    public static void deleteDeck( String deckName ){

        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){

            if(deck.getName().equals(deckName)){

                player.getAllDecks().remove(deck);
                return;
            }

        }
    }


    public static ArrayList<String> filterByMana(ArrayList<String> cardNames, int manaCost){

        ArrayList<String> filteredCards = new ArrayList<>();

        for(String cardName : cardNames){

            if(CardProducible.produceCardObject(cardName).getMana() == manaCost)
                filteredCards.add(cardName);

        }

        return filteredCards;


    }

    public static ArrayList<String> showMyCardsFilter(ArrayList<String> cardNames){
        ArrayList<String> filteredCards = new ArrayList<>();

        for(String cardName : cardNames){

            if(isCardAvailable(cardName))
                filteredCards.add(cardName);

        }
        return filteredCards;
    }

    public static ArrayList<String> showNotMyCardsFilter(ArrayList<String> cardNames){
        ArrayList<String> filteredCards = new ArrayList<>();

        for(String cardName : cardNames){


            if( !isCardAvailable(cardName))
                filteredCards.add(cardName);

        }
        return filteredCards;
    }

    public static ArrayList<String> getCardsOfDeck(String deckName){
        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(deckName))
                return deck.getCards();
        }
        ArrayList<String> o = null;
        return o;

    }


    public static ArrayList<String> getAvailableCards(){
        Player player = GameState.getInstance().getPlayer();
        return player.getAvailableCards();

    }


    public static boolean renameDeckName(String newName, String oldName){
        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(newName))
                return false;
        }
        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(oldName))
                deck.setName(newName);

            try {
                Log.writeBodyInLogFile(oldName + " RenameTo", newName);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }


        return false;
    }



    public static boolean changeHero(String deckName, String newHeroName){
        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(deckName)){
                System.out.println("deck barabar ba " + deckName + " peida shod");
                if(isChangeHeroPossible(deck, newHeroName)){
                    deck.setHeroName(newHeroName);

                    try {
                        Log.writeBodyInLogFile(deckName + " ChangeHero", newHeroName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return true;
                }
            }

        }
        return false;
    }

    public static boolean isChangeHeroPossible(Deck deck, String newHeroName) {
        if(deck.getHeroName().equals(newHeroName))
            return true;
        for (String cardName : deck.getCards()) {
            if (!CardProducible.produceCardObject(cardName).getClassOfCard().equals("Neutral")) {
                return false;
            }
        }
        return true;
    }


    public static void chooseCurrentDeck(String deckName, Player player){

        for(Deck deck : player.getAllDecks()){
            if(deck.getName().equals(deckName)){
                player.setCurrentDeck(deck);
                player.setCurrentHero(deck.getHeroName());
                player.setHasChosenDeck(true);
                player.setHasChosenHero(true);

                try {
                    Log.writeBodyInLogFile("ChooseCurrentDeck", deckName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    public static void removeCardFromDeck(String cardName, String deckName){

        Player player = GameState.getInstance().getPlayer();

        for(Deck deck : player.getAllDecks()){

            if(deck.getName().equals(deckName)){


                try {
                    Log.writeBodyInLogFile(cardName + "RemovedFrom", deckName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    public static ArrayList<String> searchedArrayList(ArrayList<String> cardNames, String word){

        ArrayList<String> resultArrayList = new ArrayList<>();

        for(String cardName : cardNames){
            if(cardName.contains(word)){
                resultArrayList.add(cardName);
            }
        }
        return resultArrayList;
    }





}
