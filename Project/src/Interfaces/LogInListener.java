package Interfaces;
import Models.Player;

import java.util.EventListener;

public interface LogInListener extends EventListener {

    Player getPlayerFromLogInPanel(Player player);

}
