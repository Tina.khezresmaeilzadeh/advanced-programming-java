package Util;

import Interfaces.Movable;
import Logic.FinishStates;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyKeyListener implements KeyListener, Movable {
    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

        if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {

            Movable.moveByKeys("right");

        } else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {

            Movable.moveByKeys("left");


        } else if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {

            Movable.moveByKeys("up");

        } else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {

            Movable.moveByKeys("down");



            if (FinishStates.getGameState().equals("finished")) {
                return;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
