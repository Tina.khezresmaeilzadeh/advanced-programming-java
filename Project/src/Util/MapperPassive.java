package Util;

import java.awt.*;
import java.util.ArrayList;

public class MapperPassive {

    private static ArrayList<String> passiveItems = new ArrayList<>();
    private static ArrayList<Rectangle> passiveItemsRectangles = new ArrayList<>();
    private static String passiveNameSelected = "";


    {
        passiveItems.add("Twice draw");
        passiveItems.add("Off cards");
        passiveItems.add("Nurse");
        passiveItems.add("free power");
        passiveItems.add("Mana jump");
        passiveItems.add("Zambie");


    }



    public static ArrayList<String> getPassiveItems() {
        return passiveItems;
    }

    public static void setPassiveItems(ArrayList<String> passiveItems) {
        MapperPassive.passiveItems = passiveItems;
    }

    public static ArrayList<Rectangle> getPassiveItemsRectangles() {
        return passiveItemsRectangles;
    }

    public static void setPassiveItemsRectangles(ArrayList<Rectangle> passiveItemsRectangles) {
        MapperPassive.passiveItemsRectangles = passiveItemsRectangles;
    }

    public static String getPassiveNameSelected() {
        return passiveNameSelected;
    }

    public static void setPassiveNameSelected(String passiveNameSelected) {
        MapperPassive.passiveNameSelected = passiveNameSelected;
    }
}
