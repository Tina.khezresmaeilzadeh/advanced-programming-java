package SwingGUI.shopPanel;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import Util.Constants;
import Util.ImageLoading;
import Util.Log;
import Util.MapperShop;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ShopManagementPanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;
    private Drawer drawer;

    private Rectangle menu = new Rectangle(70, 600, 70, 40);
    private Rectangle exit = new Rectangle(150, 600, 70, 40);
    private Rectangle buyButton = new Rectangle(100, 300, 120, 60);
    private Rectangle sellButton = new Rectangle(100, 400, 120, 60);



    public ShopManagementPanel() {
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.SHOP_MANAGEMENT_PANEL_BACKGROUND_IMAGE_ADDRESS);

        drawer = new Drawer();
        addMouseListener(this);

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.COLLECTION_FILTER_PANEL_WIDTH,
                Constants.COLLECTION_FILTER_PANEL_HEIGHT, null);

        drawer.drawShopManagementPanel(g2d);






    }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        int x = mouseEvent.getX();
        int y = mouseEvent.getY();

        if(menu.contains(x, y)){

            try {
                Log.writeBodyInLogFile("Clicked", "MenuButton");
            } catch (IOException e) {
                e.printStackTrace();
            }
            MainFrame.showMenuPanel();

        }else if(exit.contains(x, y)){

            try {
                Log.writeBodyInLogFile("Clicked", "ExitButton");
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Exit
            System.exit(0);

        }else if(buyButton.contains(x, y)) {

            try {
                Log.writeBodyInLogFile("Clicked", "BuyButton");
            } catch (IOException e) {
                e.printStackTrace();
            }

            ShopPanel.setBuyState(true);
            ShopPanel.setSellState(false);
            MapperShop.produceCurrentCards(ShopPanel.isBuyState(), ShopPanel.isSellState());
            ShopPanel.shopCardPanel.repaint();

        }else if(sellButton.contains(x, y)){

            try {

                Log.writeBodyInLogFile("Clicked", "SellButton");
            } catch (IOException e) {
                e.printStackTrace();
            }


            ShopPanel.setSellState(true);
            ShopPanel.setBuyState(false);
            MapperShop.produceCurrentCards(ShopPanel.isBuyState(), ShopPanel.isSellState());
            ShopPanel.shopCardPanel.repaint();


        }



    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
