package Models.Card;

import Interfaces.Visitable;
import Logic.PlayActions.DoPlay;

public class SpellCard implements Visitable {
    private boolean questAndReward;

    public SpellCard(boolean questAndReward) {
        this.questAndReward = questAndReward;
    }


    @Override
    public void accept(DoPlay doPlay) {
        doPlay.play(this);
    }
}
