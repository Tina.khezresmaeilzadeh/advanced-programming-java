package Logic;
import Interfaces.CardProducible;
import Models.Card.Card;
import Models.Card.Deck;
import Models.Player;
import Util.Log;

import java.io.IOException;

public class ShopLogic implements CardProducible {

    public static String buyCard( String cardName){

        Player player = GameState.getInstance().getPlayer();

        Card card = CardProducible.produceCardObject(cardName);

        if( player.getNumberOfManas() >= card.getMana() ){

            player.getAvailableCards().add(cardName);

            if(player.getCurrentDeck().getHeroName().equals("Mage") && card.getType().equals("Spell"))
                player.setNumberOfManas(player.getNumberOfManas() - card.getMana() + 2);
            else
                player.setNumberOfManas(player.getNumberOfManas() - card.getMana() );

            try {
                Log.writeBodyInLogFile("SuccessfulBuy", cardName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }else{
            try {
                Log.writeBodyInLogFile("UnsuccessfulBuy", cardName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return " You do not have enough money to buy this !";


        }

    }

    public static void sellCard(String cardName){

        Player player = GameState.getInstance().getPlayer();
        Card card = CardProducible.produceCardObject(cardName);
        player.getAvailableCards().remove(cardName);
        player.setNumberOfManas(player.getNumberOfManas() + card.getMana());
        try {
            Log.writeBodyInLogFile("Sell", cardName);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
