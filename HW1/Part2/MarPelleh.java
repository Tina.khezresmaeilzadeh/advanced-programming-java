import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class MarPelleh {
    public static void main(String[] args) {
        //create a Random object by a seed read from console
        Scanner scanner = new Scanner(System.in);
        //Create a marPelleh with 2 players and get those players
        Board board = new Board(2);
        Player player1 = board.getPlayers().get(0);
        Player player2 = board.getPlayers().get(1);

        //movements and printing results
        for (int i = 0; i < 25; i++) {
            board.movePlayer(0, scanner.nextInt(), scanner.nextInt());
            board.movePlayer(1, scanner.nextInt(), scanner.nextInt());

            System.out.println("Player1 is in " + player1.getCurrentCell());
            System.out.println("Player2 is in " + player2.getCurrentCell());
            System.out.println("Total score of player1 is " + player1.getTotalScore());
            System.out.println("Total score of player2 is " + player2.getTotalScore());
            System.out.println("ScoreFromLadder_Snake of player1 is " +
                    player1.getScoreFromLadder_Snake());
            System.out.println("ScoreFromLadder_Snake of player2 is " +
                    player2.getScoreFromLadder_Snake());
            System.out.println("Player1 is better than player2 : " + P1greaterThanP2(player1, player2));


        }
    }

    public static boolean P1greaterThanP2(Player player1, Player player2) {
        int result = player1.compareTo(player2);
        if (result > 0)
            return true;
        else
            return false;
    }
}

class Board {
    private Cell[][] cells;
    private ArrayList<Player> players;
    private ArrayList<Transmitter> transmitters;

    public Board(int numberOfPlayers) {
        initCells();
        initTransmitters();
        updateTransmittersOfCells();
        initPlayers(numberOfPlayers);
    }

    //initialize methods
    private void initCells() {
        cells = new Cell[10][10];
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                cells[i][j] = new Cell(i, j);
    }

    private void initTransmitters() {
        /*
        Notice that you must use cells of board class and you
        can't create new cell in your code.You can only use methods there
        exist.

        Create five ladders here and add them to "transmitters" list
        1.A badLadder from (0,3) to (5,6)
        2.A badLadder from (3,4) to (7,8)
        3.A badLadder from (5,8) to (9,4)
        4.A goodLadder from (0,9) to (6,9)
        5.A goodLadder from (4,6) to (8,5)

        Also create five snakes here and add them to "transmitters" list
        1.A badSnake from (7,3) to (0,4)
        2.A badSnake from (8,8) to (5,5)
        3.A goodSnake from (9,3) to (1,1)
        4.A goodSnake from (4,4) to (2,9)
        5.A goodSnake from (8,6) to (5,7)
         */
        transmitters = new ArrayList<>();
        //Cell Cell1 = getCell(0, 3);
        //Cell Cell2 = getCell(5, 6);
        Transmitter badLadder1 = new BadLadder(getCell(0, 3), getCell(5, 6));
        getCell(0,3).setTransmitter(badLadder1);
        transmitters.add(badLadder1);
        //Cell Cell3 = getCell(3, 4);
        //Cell Cell4 = getCell(7, 8);
        Transmitter badLadder2 = new BadLadder(getCell(3, 4), getCell(7, 8));
        getCell(3,4).setTransmitter(badLadder2);
        transmitters.add(badLadder2);
        //Cell Cell5 = getCell(5, 8);
        //Cell Cell6 = getCell(9, 4);
        Transmitter badLadder3 = new BadLadder(getCell(5, 8), getCell(9, 4));
        getCell(5,8).setTransmitter(badLadder3);
        transmitters.add(badLadder3);
        //Cell Cell7 = getCell(0, 9);
        //Cell Cell8 = getCell(6, 9);
        Transmitter goodLadder1 = new GoodLadder(getCell(0, 9), getCell(6, 9));
        getCell(0,9).setTransmitter(goodLadder1);
        transmitters.add(goodLadder1);
        //Cell Cell9 = getCell(4, 6);
        //Cell Cell10 = getCell(8, 5);
        Transmitter goodLadder2 = new GoodLadder(getCell(4, 6), getCell(8, 5));
        getCell(4,6).setTransmitter(goodLadder2);
        transmitters.add(goodLadder2);
        //Cell Cell11 = getCell(7, 3);
        //Cell Cell12 = getCell(0, 4);
        Transmitter badSnake1 = new BadSnake(getCell(7, 3), getCell(0, 4));
        getCell(7,3).setTransmitter(badSnake1);
        transmitters.add(badSnake1);
        //Cell Cell13 = getCell(8, 8);
        //Cell Cell14 = getCell(5, 5);
        Transmitter badSnake2 = new BadSnake(getCell(8, 8), getCell(5, 5));
        getCell(8,8).setTransmitter(badSnake2);
        transmitters.add(badSnake2);

        //Cell Cell15 = getCell(9, 3);
        //Cell Cell16 = getCell(1, 1);
        Transmitter goodSnake1 = new GoodSnake(getCell(9, 3), getCell(1, 1));
        getCell(9,3).setTransmitter(goodSnake1);
        transmitters.add(goodSnake1);

        //Cell Cell17 = getCell(4, 4);
        //Cell Cell18 = getCell(2, 9);
        Transmitter goodSnake2 = new GoodSnake(getCell(4, 4), getCell(2, 9));
        getCell(4, 4).setTransmitter(goodSnake2);
        transmitters.add(goodSnake2);
        //Cell Cell19 = getCell(8, 6);
        //Cell Cell20 = getCell(5, 7);
        Transmitter goodSnake3 = new GoodSnake(getCell(8, 6), getCell(5, 7));
        getCell(8,6).setTransmitter(goodSnake3);
        transmitters.add(goodSnake3);



        //YOUR CODE HERE
    }

    private void updateTransmittersOfCells() {
        for (Transmitter transmitter : transmitters)
            transmitter.getFirstCell().setTransmitter(transmitter);
    }

    private void initPlayers(int numberOfPlayers) {
        players = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++)
            players.add(new Player(getCell(0, 0)));
    }

    //normal methods
    public void movePlayer(int indexOfPlayer, int newX, int newY) {
        Player currentPlayer = getPlayers().get(indexOfPlayer);
        Cell newCell = getCell(newX, newY);
        currentPlayer.setCurrentCell(newCell);
        checkIfMustTransmit(currentPlayer);
    }

    private void checkIfMustTransmit(Player player) {
        Transmitter transmitter = player.getCurrentCell().getTransmitter();






        if (transmitter != null) {
            transmitter.transmit(player);
        }
    }

    //getter, setter methods
    public Cell getCell(int x, int y) {
        return cells[x][y];
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
}

class Cell {
    private int x, y;
    private Transmitter transmitter;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //setter, getter methods
    public void setTransmitter(Transmitter transmitter) {
        this.transmitter = transmitter;
    }

    public Transmitter getTransmitter() {
        return transmitter;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    //Override methods
    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }
}

class Player implements Comparable<Player> {
    private Cell currentCell;
    private int scoreFromLadder_Snake = 0;

    public Player(Cell currentCell) {
        this.currentCell = currentCell;
    }

    //getter, setter methods
    public Cell getCurrentCell() {
        return currentCell;
    }

    public void setCurrentCell(Cell currentCell) {
        this.currentCell = currentCell;
    }

    public void setScoreFromLadder_Snake(int scoreFromLadder_Snake) {
        this.scoreFromLadder_Snake = scoreFromLadder_Snake;
    }

    public int getScoreFromLadder_Snake() {
        return scoreFromLadder_Snake;
    }

    public int getTotalScore() {
        return getCurrentCell().getX() + getCurrentCell().getY()
                + getScoreFromLadder_Snake();
    }

    public int compareTo(Player anotherPlayer)
    {
        return this.getTotalScore()- anotherPlayer.getTotalScore();
    }
}

abstract class Transmitter {
    Cell FirstCell;
    Cell LastCell;
    static int Number_Of_Ladders = 0;
    static int Number_Of_Good_Snakes = 0;
    static int Number_Of_Bad_Snakes = 0;

    public static int getNumber_Of_Good_Snakes() {
        return Number_Of_Good_Snakes;
    }

    public static int getNumber_Of_Bad_Snakes() {
        return Number_Of_Bad_Snakes;
    }
    public static void setNumber_Of_Good_Snakes(int number_Of_Good_Snakes) {
        Number_Of_Good_Snakes = number_Of_Good_Snakes;
    }

    public static void setNumber_Of_Bad_Snakes(int number_Of_Bad_Snakes) {
        Number_Of_Bad_Snakes = number_Of_Bad_Snakes;
    }

    public static int getNumber_Of_Ladders() {
        return Number_Of_Ladders;
    }

    public static void setNumber_Of_Ladders(int number_Of_Ladders) {
        Number_Of_Ladders = number_Of_Ladders;
    }


    //YOUR CODE HERE
    public abstract void transmit(Player player);

    public Cell getFirstCell() {
        return FirstCell;
        //YOUR CODE HERE
    }

    public Transmitter(Cell firstCell, Cell lastCell) {
        FirstCell = firstCell;
        LastCell = lastCell;
    }
}

abstract class Snake extends Transmitter {
    static int counter_check_BadSnake = 0;



    public static int getCounter_check_BadSnake() {
        return counter_check_BadSnake;
    }



    public static void setCounter_check_BadSnake(int counter_check_BadSnake) {
        Snake.counter_check_BadSnake = counter_check_BadSnake;
    }



    public Snake(Cell firstCell, Cell lastCell) {
        super(firstCell, lastCell);
    }


    //YOUR CODE HERE
}

class BadSnake extends Snake {

    public BadSnake(Cell firstCell, Cell lastCell) {
        super(firstCell, lastCell);

    }

    public void transmit(Player player)
    {

        player.setCurrentCell(LastCell);
        setNumber_Of_Bad_Snakes(getNumber_Of_Bad_Snakes() + 1);

        if(getCounter_check_BadSnake() < 7)
        {
            player.setScoreFromLadder_Snake(player.getScoreFromLadder_Snake() - 1);
            setCounter_check_BadSnake(getCounter_check_BadSnake() + 1);

        }

    }


    //YOUR CODE HERE
}

class GoodSnake extends Snake {
    public GoodSnake(Cell firstCell, Cell lastCell) {
        super(firstCell, lastCell);
    }

    public void transmit(Player player)
    {

        if( (getNumber_Of_Bad_Snakes() > getNumber_Of_Good_Snakes()) )
        {//|| (getNumber_Of_Bad_Snakes()==0 && getNumber_Of_Good_Snakes()==0)
            setNumber_Of_Good_Snakes(getNumber_Of_Good_Snakes() + 1);
            player.setCurrentCell(LastCell);
        }

    }
    //YOUR CODE HERE
}

abstract class Ladder extends Transmitter {
    static int counter_check_GoodLadder = 0;
    public static int getCounter_check_GoodLadder() {
        return counter_check_GoodLadder;
    }

    public static void setCounter_check_GoodLadder(int counter_check_GoodLadder) {
        Ladder.counter_check_GoodLadder = counter_check_GoodLadder;
    }

    public Ladder(Cell firstCell, Cell lastCell) {
        super(firstCell, lastCell);
    }
    //YOUR CODE HERE
}

class BadLadder extends Ladder {
    public BadLadder(Cell firstCell, Cell lastCell) {
        super(firstCell, lastCell);
    }

    public void transmit(Player player)
    {

        if( (getNumber_Of_Bad_Snakes() + getNumber_Of_Good_Snakes() > getNumber_Of_Ladders())  )
        {//|| (getNumber_Of_Bad_Snakes()==0 && getNumber_Of_Good_Snakes()==0 && getNumber_Of_Ladders()==0)
            player.setCurrentCell(LastCell);
            setNumber_Of_Ladders(getNumber_Of_Ladders()+1);
        }

    }
    //YOUR CODE HERE
}

class GoodLadder extends Ladder {


    public GoodLadder(Cell firstCell, Cell lastCell) {
        super(firstCell, lastCell);
    }

    public void transmit(Player player)
    {

        if( (getNumber_Of_Bad_Snakes() + getNumber_Of_Good_Snakes() > getNumber_Of_Ladders())  )

        {//|| (getNumber_Of_Bad_Snakes()==0 && getNumber_Of_Good_Snakes()==0 && getNumber_Of_Ladders()==0)

            player.setCurrentCell(LastCell);

            if(getCounter_check_GoodLadder() < 6)
            {

                player.setScoreFromLadder_Snake(player.getScoreFromLadder_Snake() + 1);
                setCounter_check_GoodLadder(getCounter_check_GoodLadder()+1);


            }
            setNumber_Of_Ladders(getNumber_Of_Ladders()+1);
        }


    }
    //YOUR CODE HERE
}