package Interfaces;
import Models.Card.Card;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public interface CardProducible {
     static Card produceCardObject(String cardName){
          Gson gson = new GsonBuilder().setPrettyPrinting().create();
          try (Reader reader = new FileReader(
                  "src/CardsInformation/"+ cardName +".json")) {

               Card card = gson.fromJson(reader, Card.class);
               return card;

          } catch (IOException e) {
               e.printStackTrace();
               return null;

          }
     }
}
