package SwingGUI.collectionPanel;

import Logic.CollectionLogic;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import Util.Constants;
import Util.ImageLoading;
import Util.MapperCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class HeroCardPanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;
    private Drawer drawer;



    public HeroCardPanel() {
        addMouseListener(this);
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.CardPANEL_BACKGROUND_IMAGE_ADDRESS);
        drawer = new Drawer();

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;


        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.COLLECTION_CARD_PANEL_WIDTH,
                Constants.COLLECTION_CARD_PANEL_HEIGHT, null);





            if(CollectionCardManagement.isShowHeros()){

               CollectionCardManagement.setHeroNamesAndPositions(drawer.drawAllHeros(g2d));

            }
            else if(CollectionCardManagement.isShowHeroCards()){

                CollectionCardManagement.setCardNamesAndPositions(
                        drawer.drawCards(g2d,
                                MapperCollection.getCurrentShowingCards(), true));

            }

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        int x = mouseEvent.getX();
        int y = mouseEvent.getY();
        if(CollectionCardManagement.isShowHeros()){

            CollectionCardManagement.getHeroNamesAndPositions().forEach((k, v) -> {
                if( v.contains(x, y)){

                    MapperCollection.produceCurrentHeroCards(k);
                    CollectionCardManagement.setShowHeros(false);
                    CollectionCardManagement.setShowHeroCards(true);
                    CollectionCardManagement.setHeroNameClicked(k);
                    CollectionCardManagement.heroCardPanel.repaint();

                }
            });

        }else if(CollectionCardManagement.isShowHeroCards()){

            CollectionCardManagement.getCardNamesAndPositions().forEach((k, v) -> {
                if( v.contains(x, y)){

                    if(CollectionLogic.isCardAvailable(k)){
                        MainFrame.showShopPanel();
                    }
                    CollectionCardManagement.setCardNameClicked(k);
                    CollectionCardManagement.heroCardPanel.repaint();

                }
            });





        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
