package SwingGUI.statusPanel;

import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import Util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class StatusCardPanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;
    private Drawer drawer;

    private Rectangle exit = new Rectangle(1190, 670, 140, 70 );
    private Rectangle menu = new Rectangle(30, 670, 140, 70 );




    public StatusCardPanel() {
        addMouseListener(this);
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.STATUS_CARD_PANEL_BACKGROUND_IMAGE_ADDRESS);
        drawer = new Drawer();


    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;


        // Background
        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.STATUS_CARD_PANEL_WIDTH,
                Constants.STATUS_CARD_PANEL_HEIGHT, null);

        // Title
        g2d.setColor(Color.ORANGE);
        g2d.setFont(new Font("Courier New", Font.BOLD, 50));
        g2d.drawString("Status", 600, 40);


        MapperStatus mapperStatus = new MapperStatus();
        drawer.drawDecksForStatus(g2d, new MapperStatus().getTenBestDecks());

        //Buttons
        drawer.drawExitAndMenuButtons(g2d);



    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int x = mouseEvent.getX();
        int y = mouseEvent.getY();

        if(menu.contains(x, y)){
            try {
                Log.writeBodyInLogFile("CLICK", "menuButton");
            } catch (IOException e) {
                e.printStackTrace();
            }
            MainFrame.showMenuPanel();
        }else if(exit.contains(x, y)){
            try {

                Log.writeBodyInLogFile("exit", "From_Status");
                Log.saveThePlayer();
                System.exit(0);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }



    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
