package Models.Card;

import Models.Hero.Hero;

public class Card  {
    private String name;
    private String nameOnCard;
    private int mana;
    private String rarity;
    private String classOfCard;
    private String type;
    private String description;
    private boolean activeTurn = false;
    private Property property;


    private SpellCard spellCard;
    private MinionCard minionCard;
    private WeaponCard weaponCard;

    public Card(String name,String nameOnCard, int mana, String rarity, String classOfCard, String type, String description) {
        this.name = name;
        this.nameOnCard = nameOnCard;
        this.mana = mana;
        this.rarity = rarity;
        this.classOfCard = classOfCard;
        this.type = type;
        this.description = description;
        if(classOfCard.equals("Spell"))
            this.activeTurn = true;
    }

    public void attack(Hero hero){

    }
    public void attack(Card card){

    }



    public String getName() {
        return name;
    }

    public void setSpellCard(SpellCard spellCard) {
        this.spellCard = spellCard;
    }

    public void setMinionCard(MinionCard minionCard) {
        this.minionCard = minionCard;
    }

    public void setWeaponCard(WeaponCard weaponCard) {
        this.weaponCard = weaponCard;
    }

    public int getMana() {
        return mana;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public String getClassOfCard() {
        return classOfCard;
    }

    public void setClassOfCard(String classOfCard) {
        this.classOfCard = classOfCard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MinionCard getMinionCard() {
        return minionCard;
    }

    public WeaponCard getWeaponCard() {
        return weaponCard;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    @Override
    public String toString() {
        return name;
    }


}
