package Util;

import Logic.CollectionLogic;
import Logic.GameState;
import SwingGUI.collectionPanel.CollectionCardManagement;
import SwingGUI.collectionPanel.CollectionDeckPanel;
import SwingGUI.collectionPanel.DeckCardPanel;
import SwingGUI.collectionPanel.DeckManagementPanel;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MapperCollection {

    private static ArrayList<String> currentShowingCards = new ArrayList<>();
    private static ArrayList<String> cardsWithoutAnyFilter = new ArrayList<>();
    private static ArrayList<String> currentDeckCards = new ArrayList<>();
    private static ArrayList<String> allAvailableCards = new ArrayList<>();

    private static ArrayList<String> currentDrawnCards = new ArrayList<>();
    private static ArrayList<Rectangle> currentDrawnRectangles = new ArrayList<>();

    public static ArrayList<String> getCurrentDrawnCards() {
        return currentDrawnCards;
    }

    public static void setCurrentDrawnCards(ArrayList<String> currentDrawnCards) {
        MapperCollection.currentDrawnCards = currentDrawnCards;
    }

    public static ArrayList<Rectangle> getCurrentDrawnRectangles() {
        return currentDrawnRectangles;
    }

    public static void setCurrentDrawnRectangles(ArrayList<Rectangle> currentDrawnRectangles) {
        MapperCollection.currentDrawnRectangles = currentDrawnRectangles;
    }

    public static ArrayList<String> getCurrentShowingCards() {
        return currentShowingCards;
    }

    public static void setCurrentShowingCards(ArrayList<String> currentShowingCards) {
        MapperCollection.currentShowingCards = currentShowingCards;
    }

    public static ArrayList<String> getCurrentDeckCards() {
        return currentDeckCards;
    }

    public static void setCurrentDeckCards(ArrayList<String> currentDeckCards) {
        MapperCollection.currentDeckCards = currentDeckCards;
    }

    public static ArrayList<String> getAllAvailableCards() {
        return allAvailableCards;
    }

    public static void setAllAvailableCards(ArrayList<String> allAvailableCards) {
        MapperCollection.allAvailableCards = allAvailableCards;
    }

    public static void produceCurrentHeroCards(String heroName){

        try {
            Log.writeBodyInLogFile("ProduceCurrentHeroCards", heroName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        currentShowingCards = CollectionLogic.giveHeroCards(heroName);
            cardsWithoutAnyFilter = CollectionLogic.giveHeroCards(heroName);


    }
    public static void filterOrUnfilterCards(
            boolean filterByMana, boolean filteredShowingAvailableCards, boolean filterShowingNotAvailableCards, int Mana){

        currentShowingCards = new ArrayList<>(cardsWithoutAnyFilter);
        if(filterByMana){
            currentShowingCards = CollectionLogic.filterByMana(cardsWithoutAnyFilter,Mana);

            try {
                Log.writeBodyInLogFile("FilteredByMana", Integer.toString(Mana));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(filteredShowingAvailableCards){

            currentShowingCards = CollectionLogic.showMyCardsFilter(currentShowingCards);

            try {
                Log.writeBodyInLogFile("Filtered", "AvailableCards");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(filterShowingNotAvailableCards){

            currentShowingCards = CollectionLogic.showNotMyCardsFilter(currentShowingCards);

            try {
                Log.writeBodyInLogFile("Filtered", "NotAvailableCards");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void produceCurrentDeckCards(String deckName){

        currentDeckCards = CollectionLogic.getCardsOfDeck(deckName);

        try {
            Log.writeBodyInLogFile("ProduceCurrentDeckCards", deckName);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public static void produceAllAvailableCards(){
        allAvailableCards = CollectionLogic.getAvailableCards();
    }

    /*
    private static boolean renameState = false;
    private static boolean deleteDeckState = false;
    private static boolean changeHeroState = false;
    private static boolean chooseCurrentDeckState = false;

     */
    public static void doCorrectActionForClickingOnDeck(String deckName, String newDeckName, String newHeroName){
        if(DeckManagementPanel.isRenameState()){

            if(DeckManagementPanel.isOkButtonClicked()) {
                if (!CollectionLogic.renameDeckName(newDeckName, DeckCardPanel.getSelectedDeckName())) {
                    CollectionDeckPanel.setMessage("This is not a valid Name.try again.");
                }
            }
            else
                CollectionDeckPanel.setMessage("Please Choose a name");

            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.setShowAllAvailableCards(false);


        }else if(DeckManagementPanel.isDeleteDeckState()){

            CollectionLogic.deleteDeck(deckName);
            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.setShowAllAvailableCards(false);
            DeckManagementPanel.setDeleteDeckState(false);

            CollectionDeckPanel.setMessage(deckName + " has been deleted.");

        }else if(DeckManagementPanel.isChangeHeroState()){

            if(DeckManagementPanel.isOkButtonClicked()){
                boolean result = CollectionLogic.changeHero(deckName, newHeroName);
                if(!result){
                    CollectionDeckPanel.setMessage("That is not possible");
                }

            }
            else
                CollectionDeckPanel.setMessage("Please Choose a name");

            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.setShowAllAvailableCards(false);






        }else if(DeckManagementPanel.isChooseCurrentDeckState()){

            CollectionLogic.chooseCurrentDeck(deckName, GameState.getInstance().getPlayer());
            CollectionCardManagement.setShowDecks(true);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.setShowAllAvailableCards(false);
            CollectionDeckPanel.setMessage("Your Current Deck is : " + deckName);



        }else if(DeckManagementPanel.isAddCardState()){
            CollectionCardManagement.setShowDecks(false);
            CollectionCardManagement.setShowDeckCards(false);
            CollectionCardManagement.setShowAllAvailableCards(true);
            produceAllAvailableCards();
        } else{
            CollectionCardManagement.setShowDecks(false);
            CollectionCardManagement.setShowDeckCards(true);
            CollectionCardManagement.setShowAllAvailableCards(false);
            produceCurrentDeckCards(deckName);
        }

        CollectionDeckPanel.deckCardPanel.repaint();
    }


    public static void removeCard(String cardName, String selectedDeckName){
        if(DeckManagementPanel.isRemoveCardState()){

            CollectionLogic.removeCardFromDeck(cardName, selectedDeckName);
            CollectionDeckPanel.deckCardPanel.repaint();

        }
    }


    public static void filterSearch(String containedName){

        currentShowingCards = CollectionLogic.searchedArrayList(cardsWithoutAnyFilter, containedName);

    }




}
