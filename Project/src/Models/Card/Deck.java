package Models.Card;

import java.util.ArrayList;
import java.util.Random;

public class Deck implements Comparable<Deck>{

    private String name;
    private ArrayList<String> cards;
    private int numOfPlays = 0;
    private int numOfWins = 0;
    private double winsToPlays = 0;
    private double meanOfManas = 0;
    private String heroName;
    private String mostFrequentCard = "";

    public Deck(String name, ArrayList<String> cards, String heroName) {
        this.name = name;
        this.cards = cards;
        this.heroName = heroName;
    }

    public Deck() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }





    public ArrayList<String> getCards() {
        return cards;
    }

    public void setCards(ArrayList<String> cards) {
        this.cards = cards;
    }

    public int getNumOfPlays() {
        return numOfPlays;
    }

    public void setNumOfPlays(int numOfPlays) {
        this.numOfPlays = numOfPlays;
    }

    public int getNumOfWins() {
        return numOfWins;
    }

    public void setNumOfWins(int numOfWins) {
        this.numOfWins = numOfWins;
    }

    public double getWinsToPlays() {
        return winsToPlays;
    }

    public void setWinsToPlays(double winsToPlays) {
        this.winsToPlays = winsToPlays;
    }

    public double getMeanOfManas() {
        return meanOfManas;
    }

    public void setMeanOfManas(double meanOfManas) {
        this.meanOfManas = meanOfManas;
    }

    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public String getMostFrequentCard() {
        return mostFrequentCard;
    }

    public void setMostFrequentCard(String mostFrequentCard) {
        this.mostFrequentCard = mostFrequentCard;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Deck deck) {
        if(this.getWinsToPlays() - deck.getWinsToPlays() > 0.01)
            return (this.getWinsToPlays() < deck.getWinsToPlays() ? -1 : 1);

        else if(this.getWinsToPlays() - deck.getWinsToPlays()< 0.01){
            if(this.getNumOfWins() > deck.getNumOfWins())
                return 1;
            else if(this.getNumOfWins() < deck.getNumOfWins())
                return -1;
            else{
                if(this.getNumOfPlays() > deck.getNumOfPlays())
                    return 1;
                else if(this.getNumOfPlays() < deck.getNumOfPlays())
                    return -1;
                else{
                    if(this.getMeanOfManas() > deck.getMeanOfManas())
                        return 1;
                    else if(this.getMeanOfManas() < deck.getMeanOfManas()){
                        return -1;

                    }


                }

            }


        }


        return new Random().nextInt(2) - 1;


    }
}
