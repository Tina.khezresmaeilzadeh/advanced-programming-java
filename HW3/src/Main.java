import GUI.MainFrame;
import Interfaces.Solvable;
import Logic.FinishStates;
import Util.*;

import javax.swing.*;


public class Main implements Solvable {

    MainFrame mainFrame = new MainFrame();

    public Main() {

        BoardConfig boardConfig = BoardConfig.getConfigInformation();
        Boolean withoutError =
                boardConfig.buildTable( MainFrame.panel.getPanelWidth(), MainFrame.panel.getPanelHeight(), mainFrame);
        mainFrame.showProperErrors(withoutError);

    }

    public static void main(String[] args) {


        Main main = new Main();


        while (true) {
            try {
                Thread.sleep(1000 / 60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            main.mainFrame.panel.repaint();
            main.mainFrame.repaint();

            if (FinishStates.isGameFinished()) {
                break;
            }
            if (FinishStates.getGameState().equals("finished")) {
                JOptionPane.showMessageDialog(main.mainFrame, "You finished the game, congratulation", "game finished",
                        JOptionPane.INFORMATION_MESSAGE);
                FinishStates.setGameFinished(true);
            }

        }


    }


}
