package SwingGUI;

import Logic.MenuLogic;
import Util.Constants;
import Util.ImageLoading;

import javax.swing.*;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MenuPanel extends JPanel  {
    private JComponentsMenu jComponentsMenu;
    private BufferedImage backgroundImage;
    private MenuLogic menuLogic ;
    private Constants constants;

    public MenuPanel() {

        menuLogic = new MenuLogic();
        constants = new Constants();
        // Setting Background
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                constants.MENU_BACKGROUND_IMAGE_ADDRESS);

        setLayout(new BorderLayout());
        // Setting JComponentsMenu
        jComponentsMenu = new JComponentsMenu();
        add(jComponentsMenu, BorderLayout.CENTER);
    }




    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, constants.FRAME_WIDTH,
                constants.FRAME_HEIGHT, null);



    }



    class JComponentsMenu extends JComponent implements ActionListener{
        private JLabel title;
        private JButton playButton;
        private JButton shopButton;
        private JButton statusButton;
        private JButton collectionButton;
        private JButton settingButton;
        private JButton exitButton;



        public JComponentsMenu() {

            //Set Up The Title
            title = new JLabel("Menu");
            title.setFont(new Font("Courier New", Font.BOLD, 50));
            title.setForeground(Color.ORANGE);


            //Set Up Play Button
            playButton = new JButton("Play");
            playButton.setFont(new Font("Courier New", Font.BOLD, 30));
            playButton.setForeground(Color.WHITE);
            playButton.setBackground(Color.BLACK);
            playButton.addActionListener(this::actionPerformed);

            //Set Up Shop Button
            shopButton = new JButton("Shop");
            shopButton.setFont(new Font("Courier New", Font.BOLD, 30));
            shopButton.setForeground(Color.WHITE);
            shopButton.setBackground(Color.BLACK);
            shopButton.addActionListener(this::actionPerformed);

            //Set Up Status Button
            statusButton = new JButton("Status");
            statusButton.setFont(new Font("Courier New", Font.BOLD, 30));
            statusButton.setForeground(Color.WHITE);
            statusButton.setBackground(Color.BLACK);
            statusButton.addActionListener(this::actionPerformed);

            //Set Up Collection Button
            collectionButton = new JButton("Collection");
            collectionButton.setFont(new Font("Courier New", Font.BOLD, 30));
            collectionButton.setForeground(Color.WHITE);
            collectionButton.setBackground(Color.BLACK);
            collectionButton.addActionListener(this::actionPerformed);

            //Set Up Setting Button
            settingButton = new JButton("Setting");
            settingButton.setFont(new Font("Courier New", Font.BOLD, 30));
            settingButton.setForeground(Color.WHITE);
            settingButton.setBackground(Color.BLACK);
            settingButton.addActionListener(this::actionPerformed);

            //Set Up Exit Button
            exitButton = new JButton("Exit");
            exitButton.setFont(new Font("Courier New", Font.BOLD, 30));
            exitButton.setForeground(Color.WHITE);
            exitButton.setBackground(Color.BLACK);
            exitButton.addActionListener(this::actionPerformed);

            // Layout For Components Part
            setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();


            // Painting Components
            // Title
            gc.weightx = 20;
            gc.weighty = 30;
            gc.gridx = 0;
            gc.gridy = 0;
            gc.fill = GridBagConstraints.NONE;
            gc.insets = new Insets(0, 0, 0, 5);
            gc.anchor = GridBagConstraints.CENTER;
            add(title, gc);

            // Play Button
            gc.gridy++;
            add(playButton, gc);

            // Shop Button
            gc.gridy++;
            add(shopButton, gc);

            // Status Button
            gc.gridy++;
            add(statusButton, gc);

            // Collection Button
            gc.gridy++;
            add(collectionButton, gc);

            // Setting Button
            gc.gridy++;
            add(settingButton, gc);

            // Exit Button
            gc.anchor = GridBagConstraints.LAST_LINE_END;
            add(exitButton, gc);




        }
        @Override
        public void actionPerformed(ActionEvent actionEvent) {

            JButton clicked = (JButton)actionEvent.getSource();
            if (playButton.equals(clicked)) {

                try {
                    menuLogic.playButtonClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (shopButton.equals(clicked)) {

                try {
                    menuLogic.shopButtonClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (statusButton.equals(clicked)) {

                try {
                    menuLogic.statusButtonClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (collectionButton.equals(clicked)) {

                try {
                    menuLogic.collectionButtonClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (settingButton.equals(clicked)) {

                try {
                    menuLogic.settingButtonClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



        }
    }



}
