package SwingGUI;
import Logic.LogInLogic;
import Util.Constants;
import Util.ImageLoading;
import Util.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;


public class LogInPanel extends JPanel{

    private JComponentsLogIn jComponentsLogIn;
    private BufferedImage backgroundImage;
    private Constants constants;
    private LogInLogic logInLogic;





    public LogInPanel() {

        logInLogic = new LogInLogic();
        constants = new Constants();
        // Setting Background
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                constants.LOGIN_BACKGROUND_IMAGE_ADDRESS);

        setLayout(new BorderLayout());
        // Setting JComponents
        jComponentsLogIn = new JComponentsLogIn();
        add(jComponentsLogIn, BorderLayout.CENTER);
    }




    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, constants.FRAME_WIDTH,
                constants.FRAME_HEIGHT, null);

    }



    class JComponentsLogIn extends JComponent{

        private JLabel title;
        private JLabel logInQuestion;
        private JLabel usernameLabel;
        private JLabel passwordLabel;
        private JTextField usernameField;
        private JTextField passwordField;
        private JRadioButton yesRadio;
        private JRadioButton noRadio;
        private ButtonGroup logInQuestionGroup;
        private JButton okButton;
        private JLabel errorPlace;
        private JButton exitButton;


        public JComponentsLogIn() {

            //Set Up Title Label
            title = new JLabel("Hearthstone");
            title.setFont(new Font("Helvetica", Font.BOLD, 80));
            title.setForeground(Color.orange);

            //Set Up Log-In Label
            logInQuestion = new JLabel("Already have account?");
            logInQuestion.setFont(new Font("Courier New", Font.BOLD, 50));
            logInQuestion.setForeground(Color.ORANGE);

            //Set Up Yes/No Radio
            yesRadio = new JRadioButton("yes");
            yesRadio.setFont(new Font("Courier New", Font.ITALIC, 40));
            yesRadio.setForeground(Color.ORANGE);
            yesRadio.setOpaque(false);

            noRadio = new JRadioButton("no");
            noRadio.setFont(new Font("Courier New", Font.ITALIC, 40));
            noRadio.setForeground(Color.ORANGE);
            noRadio.setOpaque(false);

            yesRadio.setActionCommand("yes");
            noRadio.setActionCommand("no");
            logInQuestionGroup = new ButtonGroup();
            yesRadio.setSelected(true );

            logInQuestionGroup.add(yesRadio);
            logInQuestionGroup.add(noRadio);

            //Set up Text Fields
            usernameLabel = new JLabel("Username:");
            usernameLabel.setFont(new Font("Courier New", Font.BOLD, 40));
            usernameLabel.setForeground(Color.orange);

            passwordLabel = new JLabel("Password:");
            passwordLabel.setFont(new Font("Courier New", Font.BOLD, 40));
            passwordLabel.setForeground(Color.orange);

            usernameField = new JTextField(20);
            passwordField = new JTextField(20);


            // Set up OK Button
            okButton = new JButton("OK");
            okButton.setFont(new Font("Courier New", Font.BOLD, 30));
            okButton.setForeground(Color.WHITE);
            okButton.setBackground(Color.BLACK);

            okButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    String username = usernameField.getText();
                    String password = passwordField.getText();

                    String yesOrNo  = logInQuestionGroup.getSelection().getActionCommand();
                    errorPlace.setText("");

                    switch (yesOrNo){

                        case "yes":

                            logInLogic.logInOldPlayer(username, password, errorPlace);
                            break;

                        case "no":

                            logInLogic.logInNewPlayer(username, password, errorPlace);
                            break;

                    }

                }
            });

            // Set Up Exit Button
            exitButton = new JButton("Exit");
            exitButton.setFont(new Font("Courier New", Font.BOLD, 30));
            exitButton.setForeground(Color.WHITE);
            exitButton.setBackground(Color.BLACK);
            exitButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    Log.saveThePlayer();
                    System.exit(0);

                }
            });



            // Set Up Error Handler
            errorPlace = new JLabel();
            errorPlace.setFont(new Font("Courier New", Font.ITALIC, 40));
            errorPlace.setForeground(Color.red);




            setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();

            // Paint Components
            // Title
            gc.weightx = 20;
            gc.weighty = 40;
            gc.gridx = 0;
            gc.gridy = 500;
            gc.fill = GridBagConstraints.NONE;
            gc.insets = new Insets(0, 0, 0, 5);
            gc.anchor = GridBagConstraints.CENTER;
            add(title, gc);

            // Already have account?
            gc.weightx = 1;
            gc.weighty = 20;
            gc.gridy++;
            gc.anchor = GridBagConstraints.CENTER;
            add(logInQuestion, gc);

            // Yes Radio
            gc.gridy++;
            gc.insets = new Insets(0, 0, 0, 0);
            gc.anchor = GridBagConstraints.CENTER;
            add(yesRadio, gc);

            // No Radio
            gc.gridy ++;
            gc.insets = new Insets(0, 0, 0, 0);
            gc.anchor = GridBagConstraints.CENTER;
            add(noRadio, gc);

            // Username:
            gc.gridy ++;
            gc.gridx = 0;
            gc.insets = new Insets(0, 0, 0, 10);
            gc.anchor = GridBagConstraints.CENTER;
            add(usernameLabel, gc);

            // Username Box
            gc.gridy ++;
            gc.anchor = GridBagConstraints.CENTER;
            add(usernameField, gc);

            // Password:
            gc.gridy ++;
            gc.anchor = GridBagConstraints.CENTER;
            add(passwordLabel, gc);

            // Password Box
            gc.gridy ++;
            gc.anchor = GridBagConstraints.CENTER;
            add(passwordField, gc);

            // Ok Button
            gc.gridy ++;
            gc.insets = new Insets(0, 0, 0, 10);
            gc.anchor = GridBagConstraints.CENTER;
            add(okButton, gc);

            // Error Handler
            gc.gridy ++;
            gc.anchor = GridBagConstraints.CENTER;
            add(errorPlace, gc);

            // Exit Button
            gc.anchor = GridBagConstraints.LAST_LINE_END;
            add(exitButton, gc);




        }
    }








}
