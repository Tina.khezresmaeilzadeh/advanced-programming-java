package Models.Card;

import Models.Card.act.Act;
import Models.Card.act.Effect;
import Models.Card.act.EffectOccurringPlace;
import Models.Card.act.EffectOccurringTime;
import Models.Hero.Hunter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.ArrayList;

// Card(String name,String nameOnCard, int mana, String rarity, String classOfCard, String type, String description)
public class CardJsonProducer {
    public static ArrayList<String> totalGameCards = new ArrayList<>();

    public static void main(String[] args) {

        CardJsonProducer cardJsonProducer = new CardJsonProducer();
        Property property;

        ////////////////////////////// PRIEST

        Card HighPriestAmet = new Card("HighPriestAmet", "High Priest Amet", 4, "Legendary",
                "Priest", "Minion", "Whenever you summon a minion, set its Health equal to this minions.");
        HighPriestAmet.setMinionCard(new MinionCard(7, 2));
        // Set Property
        property = new Property();
        property.setActed(true);
        property.setAct(new Act(Effect.setHealth, EffectOccurringTime.Trigger, EffectOccurringPlace.SummonedMinion,
                true, 0, "thisHealth"));
        HighPriestAmet.setProperty(property);


        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(HighPriestAmet);
        totalGameCards.add(HighPriestAmet.getName());

        ///////////////////////////////

        Card HealingTouch = new Card("HealingTouch", "Healing Touch", 3, "Common",
                "Priest", "Spell", "Restore 8 Health.");
        HealingTouch.setSpellCard(new SpellCard(false));

        // Set Property
        property = new Property();
        property.setActed(true);
        property.setAct(new Act(Effect.IncreaseHealth, EffectOccurringTime.Battlecry, EffectOccurringPlace.FriendlyHero,
                true, 8, ""));
        HealingTouch.setProperty(property);

        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(HealingTouch);
        totalGameCards.add(HealingTouch.getName());

        /////////////////////////////// HUNTER

        Card SwampKingDred = new Card("SwampKingDred", "Swamp King Dred", 7, "Legendary",
                "Hunter", "Minion", "After your opponent plays a minion, attack it.");
        SwampKingDred.setMinionCard(new MinionCard(9, 9));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(SwampKingDred);
        totalGameCards.add(SwampKingDred.getName());

        ///////////////////////////////

        Card Huffer = new Card("Huffer", "Huffer", 3, "Common",
                "Hunter", "Minion", "Charge");
        Huffer.setMinionCard(new MinionCard(2, 4));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Huffer);
        totalGameCards.add(Huffer.getName());

        /////////////////////////////// MAGE

        Card Polymorph = new Card("Polymorph", "Polymorph", 4, "Common",
                "Mage", "Spell", "Transform a minion into a 1/1 Sheep.");
        Polymorph.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Polymorph);
        totalGameCards.add(Polymorph.getName());

        ///////////////////////////////

        Card Bite = new Card("Bite", "Bite", 4, "Rare",
                "Mage", "Spell", "Give your hero +4 Attack this turn. Gain 4 Armor.");
        Bite.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Bite);
        totalGameCards.add(Bite.getName());

        /////////////////////////////// ROUGE

        Card FriendlySmith = new Card("FriendlySmith", "Friendly Smith", 1, "Common",
                "Rouge", "Spell", "Discover a weapon from any class. Add it to your Adventure Deck with +2/ +2");
        FriendlySmith.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(FriendlySmith);
        totalGameCards.add(FriendlySmith.getName());

        ///////////////////////////////

        Card Spymistress = new Card("Spymistress", "Spymistress", 1, "Common",
                "Rouge", "Minion", "Stealth");
        Spymistress.setMinionCard(new MinionCard(1, 3));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Spymistress);
        totalGameCards.add(Spymistress.getName());

        /////////////////////////////// WARLOCK

        Card Dreadscale = new Card("Dreadscale", "Dreadscale", 3, "Legendary",
                "Warlock", "Minion", "At the end of your turn, deal 1 damage to all other minions.");
        Dreadscale.setMinionCard(new MinionCard(2, 4));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Dreadscale);
        totalGameCards.add(Dreadscale.getName());

        ///////////////////////////////

        Card Hellfire = new Card("Hellfire", "Hellfire", 4, "Common",
                "Warlock", "Spell", "Deal 3 damage to ALL characters.");
        Hellfire.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Hellfire);
        totalGameCards.add(Hellfire.getName());

        ///////////////////////////////  THREE WEAPONS

        Card BloodFury = new Card("BloodFury", "Blood Fury", 3, "Common",
                "Neutral", "Weapon", "");
        BloodFury.setWeaponCard(new WeaponCard(8, 3));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(BloodFury);
        totalGameCards.add(BloodFury.getName());

        ///////////////////////////////

        Card DragonClaw = new Card("DragonClaw", "Dragon Claw", 5, "Common",
                "Neutral", "Weapon", "");
        DragonClaw.setWeaponCard(new WeaponCard(2, 5));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(DragonClaw);
        totalGameCards.add(DragonClaw.getName());

        ///////////////////////////////

        Card PoisonedDagger = new Card("PoisonedDagger", "Poisoned Dagger", 1, "Common",
                "Neutral", "Weapon", "");
        PoisonedDagger.setWeaponCard(new WeaponCard(2, 2));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(PoisonedDagger);
        totalGameCards.add(PoisonedDagger.getName());

        /////// TEN CARDS MENTIONED IN PHASE 2 DOC
        /////////////////////////////// QUEST AND REWARD

        Card StrengthInNumbers = new Card("StrengthInNumbers", "Strength In Numbers", 1, "Common",
                "Neutral", "Spell", "Sidequest: Spend 10 Mana on minions. Reward: Summon a minion from your deck.");
        StrengthInNumbers.setSpellCard(new SpellCard(true));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(StrengthInNumbers);
        totalGameCards.add(StrengthInNumbers.getName());

        ///////////////////////////////

        Card LearnDraconic = new Card("LearnDraconic", "Learn Draconic", 1, "Common",
                "Neutral", "Spell", "Sidequest: Spend 8 Mana on spells. Reward: Summon a 6/6 Dragon.");
        LearnDraconic.setSpellCard(new SpellCard(true));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(LearnDraconic);
        totalGameCards.add(LearnDraconic.getName());

        ///////////////////////////////

        Card Sathrovarr = new Card("Sathrovarr", "Sathrovarr", 9, "Legendary",
                "Neutral", "Minion", "Battlecry: Choose a friendly minion." +
                " Add a copy of it to your hand, deck, and battlefield.");
        Sathrovarr.setMinionCard(new MinionCard(5, 5));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Sathrovarr);
        totalGameCards.add(Sathrovarr.getName());

        ///////////////////////////////

        Card TombWarden = new Card("TombWarden", "Tomb Warden", 8, "Rare",
                "Neutral", "Minion", "Taunt Battlecry: Summon a copy of this minion.");
        TombWarden.setMinionCard(new MinionCard(6, 3));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(TombWarden);
        totalGameCards.add(TombWarden.getName());

        ///////////////////////////////

        Card SecurityRover = new Card("SecurityRover", "Security Rover", 6, "Rare",
                "Neutral", "Minion", "Whenever this minion takes damage, summon a 2/3 Mech with Taunt.");
        SecurityRover.setMinionCard(new MinionCard(6, 2));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(SecurityRover);
        totalGameCards.add(SecurityRover.getName());

        ///////////////////////////////

        Card CurioCollector = new Card("CurioCollector", "Curio Collector", 5, "Rare",
                "Neutral", "Minion", "Whenever you draw a card, gain +1/+1");
        CurioCollector.setMinionCard(new MinionCard(4, 4));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(CurioCollector);
        totalGameCards.add(CurioCollector.getName());

        ///////////////////////////////

        Card Sprint = new Card("Sprint", "Sprint", 7, "Common",
                "Neutral", "Spell", "Draw 4 cards.");
        Sprint.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Sprint);
        totalGameCards.add(Sprint.getName());

        ///////////////////////////////

        Card SwarmOfLocusts = new Card("SwarmOfLocusts", "Swarm Of Locusts", 6, "Rare",
                "Neutral", "Spell", "Summon seven 1/1 Locusts with Rush.");
        SwarmOfLocusts.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(SwarmOfLocusts);
        totalGameCards.add(SwarmOfLocusts.getName());

        ///////////////////////////////

        Card PharoahsBlessing = new Card("PharoahsBlessing", "Pharoahs Blessing", 6, "Rare",
                "Neutral", "Spell", "Give a minion +4/+4 Divine Shield, and Taunt.");
        PharoahsBlessing.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(PharoahsBlessing);
        totalGameCards.add(PharoahsBlessing.getName());

        ///////////////////////////////

        Card BookOfSpectors = new Card("BookOfSpectors", "Book Of Spectors", 2, "Epic",
                "Neutral", "Spell", "Draw 3 cards. Discard any spells drawn.");
        BookOfSpectors.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(BookOfSpectors);
        totalGameCards.add(BookOfSpectors.getName());

        /////////////////////////////// 14 NEUTRAL CARDS OF PHASE 1

        Card DraconicEmissary = new Card("DraconicEmissary", "Draconic Emissary", 6, "Common",
                "Neutral", "Minion", "");
        DraconicEmissary.setMinionCard(new MinionCard(6, 6));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(DraconicEmissary);
        totalGameCards.add(DraconicEmissary.getName());

        ///////////////////////////////

        Card GuardBot = new Card("GuardBot", "Guard Bot", 2, "Common",
                "Neutral", "Minion", "Taunt");
        GuardBot.setMinionCard(new MinionCard(3, 2));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(GuardBot);
        totalGameCards.add(GuardBot.getName());

        ///////////////////////////////

        Card Locust = new Card("Locust", "Locust", 1, "Common",
                "Neutral", "Minion", "Rush");
        Locust.setMinionCard(new MinionCard(1, 1));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Locust);
        totalGameCards.add(Locust.getName());

        ///////////////////////////////

        Card WrathscaleNaga = new Card("WrathscaleNaga", "Wrathscale Naga", 3, "Epic",
                "Neutral", "Minion", "After a friendly minion dies, deal 3 damage to a random enemy.");
        WrathscaleNaga.setMinionCard(new MinionCard(1, 3));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(WrathscaleNaga);
        totalGameCards.add(WrathscaleNaga.getName());

        ///////////////////////////////

        Card Alexstrasza = new Card("Alexstrasza", "Alexstrasza", 9, "Legendary",
                "Neutral", "Minion", "Battlecry: Set a heros remaining Health to 15.");
        Alexstrasza.setMinionCard(new MinionCard(8, 8));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Alexstrasza);
        totalGameCards.add(Alexstrasza.getName());

        ///////////////////////////////

        Card ManaBurn = new Card("ManaBurn", "Mana Burn", 1, "Common",
                "Neutral", "Spell", "Your opponent has 2 fewer Mana Crystals next turn.");
        ManaBurn.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(ManaBurn);
        totalGameCards.add(ManaBurn.getName());

        ///////////////////////////////

        Card Blur = new Card("Blur", "Blur", 0, "Common",
                "Neutral", "Spell", "Your hero cant take damage this turn.");
        Blur.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Blur);
        totalGameCards.add(Blur.getName());

        ///////////////////////////////

        Card AnubisathDefender = new Card("AnubisathDefender", "Anubisath Defender", 5, "Epic",
                "Neutral", "Minion", "Costs (0) if you have cost a spell that costs (5) or more this turn.");
        AnubisathDefender.setMinionCard(new MinionCard(5, 3));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(AnubisathDefender);
        totalGameCards.add(AnubisathDefender.getName());

        ///////////////////////////////

        Card Moonfire = new Card("Moonfire", "Moonfire", 0, "Common",
                "Neutral", "Spell", "Deal 1 damage.");
        Moonfire.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Moonfire);
        totalGameCards.add(Moonfire.getName());

        ///////////////////////////////

        Card BlessingOfTheAncients = new Card("BlessingOfTheAncients", "Blessing Of The Ancients", 3, "Common",
                "Neutral", "Spell", "Twinspell Give your minions +1/ +1");
        BlessingOfTheAncients.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(BlessingOfTheAncients);
        totalGameCards.add(BlessingOfTheAncients.getName());

        ///////////////////////////////

        Card Starscryer = new Card("Starscryer", "Starscryer", 2, "Common",
                "Neutral", "Minion", "Deathrattle: Draw a spell.");
        Starscryer.setMinionCard(new MinionCard(1, 3));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(Starscryer);
        totalGameCards.add(Starscryer.getName());

        ///////////////////////////////

        Card MassDispel = new Card("MassDispel", "Mass Dispel", 4, "Rare",
                "Neutral", "Spell", "Silence all enemy minions. Draw a card.");
        MassDispel.setSpellCard(new SpellCard(false));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(MassDispel);
        totalGameCards.add(MassDispel.getName());

        ///////////////////////////////

        Card ArgentSquire = new Card("ArgentSquire", "Argent Squire", 1, "Common",
                "Neutral", "Minion", "Devine Shield");
        ArgentSquire.setMinionCard(new MinionCard(1, 1));
        // Write Json File
        cardJsonProducer.writeCardOnJsonFile(ArgentSquire);
        totalGameCards.add(ArgentSquire.getName());



        //////////////////////////////////// WRITING THE LIST OF TOTAL GAME CARDS IN A JSON FILE
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter("src/GameGeneralInformation/totalGameCards.json")) {
            gson.toJson(totalGameCards, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }


        ArrayList<String> totalHeroNames = new ArrayList<>();
        totalHeroNames.add("Mage");
        totalHeroNames.add("Priest");
        totalHeroNames.add("Hunter");
        totalHeroNames.add("Rouge");
        totalHeroNames.add("Warlock");

        try (FileWriter writer = new FileWriter("src/GameGeneralInformation/totalHeroNames.json")) {
            gson.toJson(totalHeroNames, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }






















    }


        private void writeCardOnJsonFile(Card card){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter("src/CardsInformation/" + card.getName() + ".json")) {
            gson.toJson(card, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
