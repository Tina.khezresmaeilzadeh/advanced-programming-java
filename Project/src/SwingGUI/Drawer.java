package SwingGUI;
import Interfaces.CardProducible;
import Logic.CollectionLogic;
import Logic.GameState;
import Logic.PlayLogic;
import Models.Card.Card;
import Models.Card.Deck;
import Util.*;
import org.w3c.dom.css.Rect;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

public class Drawer {


    public void drawMainCollectionPanelPart(Graphics2D g2d){

        // Hero Cards Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 520, 130, 40);
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);
        g2d.drawString("Hero Cards",27,544);
        g2d.drawRect(20, 520, 130, 40 );

        // My Decks Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(170, 520, 120, 40);
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);
        g2d.drawString(" My Decks",177,544);
        g2d.drawRect(170, 520, 120, 40 );

        // Menu Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 670, 70, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Menu",27,694);
        g2d.drawRect(20, 670, 70, 40 );

        // Exit Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(120, 670, 70, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Exit",127,694);
        g2d.drawRect(120, 670, 70, 40 );


    }

    public void drawExitAndMenuButtons(Graphics2D g2d){
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));

        // Exit Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(1190, 670, 140, 70);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Exit",1230,710);
        g2d.drawRect(1190, 670, 140, 70 );

        // Menu Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(30, 670, 140, 70);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Menu",70,710);
        g2d.drawRect(30, 670, 140, 70 );

    }
    public void drawFilterPanel(Graphics2D g2d){

        g2d.setFont(new Font("Courier New", Font.BOLD, 20));


        // Search Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(140, 50, 100, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Search",150,73);
        g2d.drawRect(140, 50, 100, 40);

        // Mana Filter
        g2d.setColor(Color.YELLOW);
        g2d.drawString("Mana Filter",20, 150);


        // 0
        g2d.setColor(Color.BLUE);
        g2d.fillRect(20, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("0",24,186);
        g2d.drawRect(20, 170, 20, 20);

        // 1
        g2d.setColor(Color.BLUE);
        g2d.fillRect(50, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("1",54,186);
        g2d.drawRect(50, 170, 20, 20);
        // 2
        g2d.setColor(Color.BLUE);
        g2d.fillRect(80, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("2",84,186);
        g2d.drawRect(80, 170, 20, 20);
        // 3
        g2d.setColor(Color.BLUE);
        g2d.fillRect(110, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("3",114,186);
        g2d.drawRect(110, 170, 20, 20);
        // 4
        g2d.setColor(Color.BLUE);
        g2d.fillRect(140, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("4",144,186);
        g2d.drawRect(140, 170, 20, 20);
        // 5
        g2d.setColor(Color.BLUE);
        g2d.fillRect(170, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("5",174,186);
        g2d.drawRect(170, 170, 20, 20);
        // 6
        g2d.setColor(Color.BLUE);
        g2d.fillRect(200, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("6",204,186);
        g2d.drawRect(200, 170, 20, 20);
        // 7
        g2d.setColor(Color.BLUE);
        g2d.fillRect(230, 170, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("7",234,186);
        g2d.drawRect(230, 170, 20, 20);
        // 8
        g2d.setColor(Color.BLUE);
        g2d.fillRect(20, 200, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("8",24,216);
        g2d.drawRect(20, 200, 20, 20);
        // 9
        g2d.setColor(Color.BLUE);
        g2d.fillRect(50, 200, 20, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("9",54,216);
        g2d.drawRect(50, 200, 20, 20);
        // 10
        g2d.setColor(Color.BLUE);
        g2d.fillRect(80, 200, 30, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("10",84,216);
        g2d.drawRect(80, 200, 30, 20);
        // All
        g2d.setColor(Color.BLUE);
        g2d.fillRect(120, 200, 40, 20);
        g2d.setColor(Color.WHITE);
        g2d.drawString("All",124,216);
        g2d.drawRect(120, 200, 40, 20);


        // Mana Filter
        g2d.setColor(Color.YELLOW);
        g2d.drawString("Filter Available Cards",20, 290);



        // Player Cards
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 320, 120, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("My Cards",27,344);
        g2d.drawRect(20, 320, 120, 40);

        // Cards Player doesnt have
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 380, 120, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("New Cards",27,404);
        g2d.drawRect(20, 380, 120, 40);

        // All Cards
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 440, 120, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("All Cards",27,464);
        g2d.drawRect(20, 440, 120, 40);











    }

    public void drawDeckManagementPanel(Graphics2D g2d){
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));


        // Ok Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(140, 50, 100, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Ok",170,73);
        g2d.drawRect(140, 50, 100, 40);

        // Delete Deck
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 320, 140, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Delete Deck",27,344);
        g2d.drawRect(20, 320, 140, 40);

        // Rename
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 380, 140, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Rename",27,404);
        g2d.drawRect(20, 380, 140, 40);

        // Change Hero
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 440, 140, 40);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Change Hero",27,464);
        g2d.drawRect(20, 440, 140, 40);

        // Description
        g2d.setColor(Color.BLACK);
        int yDescription = 230;
        String order = "For doing actions ,\n please first press \nthe buttons, Then deck!";
        for (String line : order.split("\n"))
            g2d.drawString(line, 20, yDescription += g2d.getFontMetrics().getHeight());


        // Hero Cards Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(20, 150, 100, 40);
        g2d.setFont(new Font("Courier New", Font.BOLD, 15));
        g2d.setColor(Color.WHITE);
        g2d.drawString("add card",27,174);
        g2d.drawRect(20, 150, 100, 40 );

        // My Decks Button
        g2d.setColor(Color.BLACK);
        g2d.fillRect(130, 150, 120, 40);
        g2d.setFont(new Font("Courier New", Font.BOLD, 15));
        g2d.setColor(Color.WHITE);
        g2d.drawString(" remove card",137,174);
        g2d.drawRect(130, 150, 120, 40 );

        // Choose current Deck
        g2d.setColor(Color.BLACK);
        g2d.fillRect(50, 570, 190, 50);
        g2d.setFont(new Font("Courier New", Font.BOLD, 15));
        g2d.setColor(Color.WHITE);
        g2d.drawString(" Choose Current Deck",57,600);
        g2d.drawRect(50, 570, 190, 50 );






    }

    public HashMap<String, Rectangle> drawAllHeros(Graphics2D g2d) {

        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);

        ArrayList<String> heroNames = CollectionLogic.readTotalClassNames();

        HashMap<String, Rectangle> heroNamesAndRectangles = new HashMap<>();

        ImageLoading imageLoading = new ImageLoading();

        int yPhoto = 40;
        int yString = 180;
        int xPhoto = 40;
        int xString = 50;


        for (String hero : heroNames) {

            heroNamesAndRectangles.put(hero, new Rectangle(xPhoto, yPhoto, 200, 150));

            g2d.drawImage(imageLoading.loadImage(
                    Constants.HERO_IMAGES_ADDRESSES + "/" + hero + ".jpg"),
                    (int) xPhoto, (int) yPhoto, 200, 150, null);

            g2d.drawRect((int) xPhoto, (int) yPhoto, 200, 150);
            g2d.drawString(hero, xString, yString);

            xPhoto += 300;
            xString += 300;
            if (xPhoto > 800) {

                xPhoto = 40;
                xString = 50;
                yPhoto += 200;
                yString += 200;

            }


        }
        return heroNamesAndRectangles;
    }

    public HashMap<String, Rectangle> drawAllDecks(Graphics2D g2d) {

        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);

        ArrayList<Deck> decks = GameState.getInstance().getPlayer().getAllDecks();

        HashMap<String, Rectangle> deckNamesAndRectangles = new HashMap<>();

        ImageLoading imageLoading = new ImageLoading();

        int yPhoto = 40;
        int yString = 180;
        int xPhoto = 40;
        int xString = 50;


        for (Deck deck : decks) {

            deckNamesAndRectangles.put(deck.getName(), new Rectangle(xPhoto, yPhoto, 200, 150));

            g2d.drawImage(imageLoading.loadImage(
                    Constants.HERO_IMAGES_ADDRESSES + "/" + deck.getHeroName() + ".jpg"),
                    (int) xPhoto, (int) yPhoto, 200, 150, null);

            g2d.drawRect((int) xPhoto, (int) yPhoto, 200, 150);
            g2d.drawString(deck.getName(), xString, yString);

            xPhoto += 300;
            xString += 300;
            if (xPhoto > 800) {

                xPhoto = 40;
                xString = 50;
                yPhoto += 200;
                yString += 200;

            }


        }
        return deckNamesAndRectangles;
    }



    public void drawCard(Graphics2D g2d, String CardName, int xPhoto, int yPhoto){

        ImageLoading imageLoading = new ImageLoading();


            g2d.drawImage(imageLoading.loadImage(
                    Constants.CARD_IMAGES_ADDRESSES + "/" + CardName + "_full.jpg"),
                    (int) xPhoto + 40 , (int) yPhoto + 30, 200, 200, null);



            Card card = CardProducible.produceCardObject(CardName);

            if(card.getType().equals("Weapon")){

                // Card Layout
                g2d.drawImage(imageLoading.loadImage(
                        Constants.WEAPON_CARD_LAYOUT_ADDRESS),
                        (int) xPhoto, (int) yPhoto, 300, 400, null);

                // Name Of Card
                g2d.setColor(Color.ORANGE);
                g2d.setFont(new Font("Courier New", Font.BOLD, 25));
                g2d.drawString(CardName, xPhoto + 50, yPhoto + 210);

                // Mana
                g2d.setColor(Color.ORANGE);
                g2d.setFont(new Font("Courier New", Font.BOLD, 40));
                g2d.drawString(Integer.toString(card.getMana()), xPhoto + 25, yPhoto + 50);

                // Attack
                g2d.setColor(Color.ORANGE);
                g2d.setFont(new Font("Courier New", Font.BOLD, 40));
                g2d.drawString(Integer.toString(card.getWeaponCard().getAttack()), xPhoto + 35, yPhoto + 380);

                // Hero Power
                g2d.setColor(Color.ORANGE);
                g2d.setFont(new Font("Courier New", Font.BOLD, 40));
                g2d.drawString(Integer.toString(card.getWeaponCard().getDurability()), xPhoto + 260, yPhoto + 380);





            }else {


                // Card Layout
                g2d.drawImage(imageLoading.loadImage(
                        Constants.CARD_LAYOUT_ADDRESS),
                        (int) xPhoto, (int) yPhoto, 300, 400, null);

                // Name Of Card
                g2d.setColor(Color.BLACK);
                g2d.setFont(new Font("Courier New", Font.BOLD, 25));
                g2d.drawString(CardName, xPhoto + 60, yPhoto + 230);

                // Description
                g2d.setFont(new Font("Courier New", Font.BOLD, 20));
                int y = yPhoto + 250;
                for (String line : card.getDescription().split("\n")){

                    g2d.drawString(line, xPhoto + 50, y += g2d.getFontMetrics().getHeight());
                }

                // Mana
                g2d.setColor(Color.ORANGE);
                g2d.setFont(new Font("Courier New", Font.BOLD, 40));
                g2d.drawString(Integer.toString(card.getMana()), xPhoto + 25, yPhoto + 50);

                if(card.getType().equals("Minion")){

                    // Attack
                    g2d.setColor(Color.BLACK);
                    g2d.setFont(new Font("Courier New", Font.BOLD, 40));
                    g2d.drawString(Integer.toString(card.getMinionCard().getAttack()), xPhoto + 35, yPhoto + 380);

                    // Hero Power
                    g2d.setColor(Color.BLACK);
                    g2d.setFont(new Font("Courier New", Font.BOLD, 40));
                    g2d.drawString(Integer.toString(card.getMinionCard().getHealth()), xPhoto + 260, yPhoto + 380);

                }



            }

    }


    public HashMap<String, Rectangle> drawCards(Graphics2D g2d, ArrayList<String> cardNames, boolean markUnavailable){

        HashMap<String, Rectangle> cardsNamesAndRectangles = new HashMap<>();

        int xPhoto = 30;
        int yPhoto = 30;
        for(String cardName : cardNames){
            drawCard(g2d, cardName, xPhoto, yPhoto);
            if(markUnavailable){

                if( !CollectionLogic.isCardAvailable(cardName)){
                    g2d.setColor(Color.RED);

                    g2d.drawString("Unavaillable", xPhoto + 30, yPhoto + 100);
                }


            }
            cardsNamesAndRectangles.put(cardName, new Rectangle(xPhoto, yPhoto, 300, 400));
            MapperCollection.getCurrentDrawnCards().add(cardName);
            MapperCollection.getCurrentDrawnRectangles().add(new Rectangle(xPhoto, yPhoto, 300, 400));



            xPhoto += 400;
            if (xPhoto > 800) {

                xPhoto = 30;
                yPhoto += 450;

            }

        }




        /*

        g2d.drawImage(imageLoading.loadImage(
                "C:/Users/Lenovo/IdeaProjects/Asteroids-master/Projact_Ap_Phase2/src/Photos/Cards/Alexstrasza.png"),
                (int) 30, (int) 30, 300, 400, null);

         */


        return cardsNamesAndRectangles;
    }

    public void drawShopManagementPanel(Graphics2D g2d){
        // Title
        g2d.setColor(Color.ORANGE);
        g2d.setFont(new Font("Courier New", Font.BOLD, 50));
        g2d.drawString("Shop",90,50);


        g2d.setFont(new Font("Courier New", Font.BOLD, 20));

        // Menu Button
        g2d.setColor(Color.BLACK);
        g2d.drawString("Menu",77,624);
        g2d.drawRect(70, 600, 70, 40 );

        // Exit Button
        g2d.setColor(Color.BLACK);
        g2d.drawString("Exit",157,624);
        g2d.drawRect(150, 600, 70, 40 );

        // Buy Button
        g2d.setFont(new Font("Courier New", Font.BOLD, 30));
        g2d.setColor(Color.BLACK);
        g2d.drawString("Buy",127,330);
        g2d.drawRect(100, 300, 120, 60 );

        // Sell Button
        g2d.setFont(new Font("Courier New", Font.BOLD, 30));
        g2d.setColor(Color.BLACK);
        g2d.drawString("Sell",127,430);
        g2d.drawRect(100, 400, 120, 60 );



    }

    public void drawDecksForStatus(Graphics2D g2d, ArrayList<Deck> tenBestDecks){

        int x = 200;
        int y = 100;
        for( Deck deck : tenBestDecks ){

            g2d.setColor(Color.GRAY);
            g2d.drawRect(x, y, 1000, 100);

            g2d.setColor(Color.YELLOW);
            g2d.setFont(new Font("Courier New", Font.BOLD, 25));
            g2d.drawString(deck.getName(), x + 20, y + 50);

            g2d.setColor(Color.WHITE);
            g2d.setFont(new Font("Courier New", Font.BOLD, 20));
            g2d.drawString("Total Plays : " + deck.getNumOfPlays(), x + 260, y + 20);
            g2d.drawString("Wins To Plays Ratio : " + deck.getWinsToPlays() , x + 260, y + 50);
            g2d.drawString("Total Wins : " + deck.getNumOfWins() , x + 260, y + 80);

            g2d.drawString("Mean Of Manas : " + deck.getMeanOfManas() , x + 625, y + 20);
            g2d.drawString("Hero Of Deck : " + deck.getHeroName() , x + 625, y + 50);
            g2d.drawString("Most UsedCard : " + deck.getMostFrequentCard() , x + 625, y + 80);

            y = y + 120;

        }




    }

    public void drawPlayGround(Graphics2D g2d){
        // End Turn
        g2d.setColor(Color.BLACK);
        g2d.fillRect(1170, 335, 140, 70);
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);
        g2d.drawString("End Turn",1190,370);
        g2d.drawRect(1170, 335, 140, 70);

        // Friendly Manas
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);
        g2d.drawString("Mana :" + GameState.getInstance().getPlayer().getCurrentTurnResumedManas() + "/" +
                GameState.getInstance().getPlayer().getCurrentTurnTotalManas(),650,720);

        // Enemy Manas
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.setColor(Color.WHITE);
        g2d.drawString("Mana :" + GameState.getInstance().getOpponentPlayer().getCurrentTurnResumedManas() + "/" +
                GameState.getInstance().getOpponentPlayer().getCurrentTurnTotalManas(),650,40);


        // Friendly HP
        g2d.setColor(Color.blue);
        g2d.fillOval(770, 620, 40 ,40);
        g2d.setColor(Color.WHITE);
        //g2d.drawString(Integer.toString(GameState.getInstance().get.getHPHero()) , 775, 650);

        //Hand Cards


        // Friendly Remained In Deck
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.drawString(" Remained In Deck : " + PlayLogic.friendlyRemainedInDeck(), 550, 750);

        // Enemy Remained In Deck
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        g2d.drawString(" Remained In Deck : " + PlayLogic.enemyRemainedInDeck(), 550, 50);








    }


    public void drawFriendlyHandCards(Graphics2D g2d, ArrayList<String> cardNames){

        MapperPlay.setCurrentFriendlyOnHandCards(new ArrayList<>());
        MapperPlay.setCurrentFriendlyOnHandRectangles(new ArrayList<>());

        int xPhoto = 30;
        int yPhoto = 600;
        for(String cardName : cardNames) {
            drawPlayGroundCard(g2d, cardName, xPhoto, yPhoto);

            MapperPlay.getCurrentFriendlyOnHandCards().add(cardName);
            MapperPlay.getCurrentFriendlyOnHandRectangles().add(new Rectangle(xPhoto, yPhoto, 150, 200));

            xPhoto += 150;


        }


    }

    public void drawEnemyHandCards(Graphics2D g2d, ArrayList<String> cardNames){

        MapperPlay.setCurrentEnemyOnHandCards(new ArrayList<>());
        MapperPlay.setCurrentEnemyOnHandRectangles(new ArrayList<>());

        int xPhoto = 30;
        int yPhoto = 80;
        for(String cardName : cardNames) {
            drawPlayGroundCard(g2d, cardName, xPhoto, yPhoto);

            MapperPlay.getCurrentEnemyOnHandCards().add(cardName);
            MapperPlay.getCurrentEnemyOnHandRectangles().add(new Rectangle(xPhoto, yPhoto, 150, 200));

            xPhoto += 150;


        }


    }

    public void drawFriendlyOnGroundCards(Graphics2D g2d, ArrayList<String> cardNames){
        MapperPlay.setCurrentFriendlyOnPlayGroundCards(new ArrayList<>());
        MapperPlay.setCurrentFriendlyOnPlayGroundRectangles(new ArrayList<>());
        int xPhoto = 200;
        int yPhoto = 400;

        for(String cardName : cardNames) {
            drawPlayGroundCard(g2d, cardName, xPhoto, yPhoto);
            MapperPlay.getCurrentFriendlyOnPlayGroundCards().add(cardName);
            MapperPlay.getCurrentFriendlyOnPlayGroundRectangles().add(new Rectangle(xPhoto, yPhoto, 150, 200));

            xPhoto += 170;


        }

    }

    public void drawEnemyOnGroundCards(Graphics2D g2d, ArrayList<String> cardNames){
        MapperPlay.setCurrentEnemyOnPlayGroundCards(new ArrayList<>());
        MapperPlay.setCurrentEnemyOnPlayGroundRectangles(new ArrayList<>());
        int xPhoto = 200;
        int yPhoto = 200;

        for(String cardName : cardNames) {
            drawPlayGroundCard(g2d, cardName, xPhoto, yPhoto);
            MapperPlay.getCurrentEnemyOnPlayGroundCards().add(cardName);
            MapperPlay.getCurrentEnemyOnPlayGroundRectangles().add(new Rectangle(xPhoto, yPhoto, 150, 200));

            xPhoto += 170;


        }

    }

    public void drawPassives(Graphics2D g2d, MapperPassive mapperPassive){

        int xPhoto = 40;
        int yPhoto = 250;


        for(String passiveName : mapperPassive.getPassiveItems()) {

            g2d.setColor(Color.WHITE);
            g2d.drawRect(xPhoto, yPhoto, 200, 100);
            MapperPassive.getPassiveItemsRectangles().add(new Rectangle( xPhoto, yPhoto, 200, 100 ));
            g2d.setFont(new Font("Courier New", Font.BOLD, 20));

            g2d.drawString(passiveName, xPhoto + 60, yPhoto + 40);
            xPhoto += 220;

        }

        // Start The Game Button
        g2d.drawRect(540, 550, 200, 70);
        g2d.drawString("Start the game", 560, 580);

    }




    public void drawPlayGroundCard(Graphics2D g2d, String CardName, int xPhoto, int yPhoto){

        ImageLoading imageLoading = new ImageLoading();


        g2d.drawImage(imageLoading.loadImage(
                Constants.CARD_IMAGES_ADDRESSES + "/" + CardName + "_full.jpg"),
                (int) xPhoto + 20 , (int) yPhoto + 15, 100, 100, null);



        Card card = CardProducible.produceCardObject(CardName);

        if(card.getType().equals("Weapon")){

            // Card Layout
            g2d.drawImage(imageLoading.loadImage(
                    Constants.WEAPON_CARD_LAYOUT_ADDRESS),
                    (int) xPhoto, (int) yPhoto, 150, 200, null);

            // Name Of Card
            g2d.setColor(Color.ORANGE);
            g2d.setFont(new Font("Courier New", Font.BOLD, 14));
            g2d.drawString(CardName, xPhoto + 25, yPhoto + 105);

            // Mana
            g2d.setColor(Color.ORANGE);
            g2d.setFont(new Font("Courier New", Font.BOLD, 20));
            g2d.drawString(Integer.toString(card.getMana()), xPhoto + 14, yPhoto + 25);

            // Attack
            g2d.setColor(Color.ORANGE);
            g2d.setFont(new Font("Courier New", Font.BOLD, 20));
            g2d.drawString(Integer.toString(card.getWeaponCard().getAttack()), xPhoto + 17, yPhoto +190);

            // Hero Power
            g2d.setColor(Color.ORANGE);
            g2d.setFont(new Font("Courier New", Font.BOLD, 20));
            g2d.drawString(Integer.toString(card.getWeaponCard().getDurability()), xPhoto + 130, yPhoto + 190);





        }else {


            // Card Layout
            g2d.drawImage(imageLoading.loadImage(
                    Constants.CARD_LAYOUT_ADDRESS),
                    (int) xPhoto, (int) yPhoto, 150, 200, null);

            // Name Of Card
            g2d.setColor(Color.BLACK);
            g2d.setFont(new Font("Courier New", Font.BOLD, 14));
            g2d.drawString(CardName, xPhoto + 30, yPhoto + 115);

            // Description
            g2d.setFont(new Font("Courier New", Font.BOLD, 10));
            int y = yPhoto + 125;
            for (String line : card.getDescription().split("\n")){

                g2d.drawString(line, xPhoto + 25, y += g2d.getFontMetrics().getHeight());
            }

            // Mana
            g2d.setColor(Color.ORANGE);
            g2d.setFont(new Font("Courier New", Font.BOLD, 20));
            g2d.drawString(Integer.toString(card.getMana()), xPhoto + 15, yPhoto + 25);

            if(card.getType().equals("Minion")){

                // Attack
                g2d.setColor(Color.BLACK);
                g2d.setFont(new Font("Courier New", Font.BOLD, 20));
                g2d.drawString(Integer.toString(card.getMinionCard().getAttack()), xPhoto + 15, yPhoto + 190);

                // Hero Power
                g2d.setColor(Color.BLACK);
                g2d.setFont(new Font("Courier New", Font.BOLD, 20));
                g2d.drawString(Integer.toString(card.getMinionCard().getHealth()), xPhoto + 130, yPhoto + 190);

            }



        }

    }



}
