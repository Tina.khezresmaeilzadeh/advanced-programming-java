package Util;

import GUI.MainFrame;
import Interfaces.Solvable;
import Logic.FinishStates;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.tools.javac.Main;

import javax.swing.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class BoardConfig {

    private ArrayList<Integer> initialOrdering = new ArrayList<>();
    private HashMap<Integer, String> imageAddresses = new HashMap<>();

    public ArrayList<Integer> getInitialOrdering() {
        return initialOrdering;
    }

    public void setInitialOrdering(ArrayList<Integer> initialOrdering) {
        this.initialOrdering = initialOrdering;
    }

    public HashMap<Integer, String> getImageAddresses() {
        return imageAddresses;
    }

    public void setImageAddresses(HashMap<Integer, String> imageAddresses) {
        this.imageAddresses = imageAddresses;
    }

    public BoardConfig() {

    }

    /*
    public static void main(String[] args) {
        BoardConfig boardConfig = new BoardConfig();
        boardConfig.imageAddresses.put(0, "0.png");
        boardConfig.imageAddresses.put(1, "1.png");
        boardConfig.imageAddresses.put(2, "2.png");
        boardConfig.imageAddresses.put(3, "3.png");
        boardConfig.imageAddresses.put(4, "4.png");
        boardConfig.imageAddresses.put(5, "5.png");
        boardConfig.imageAddresses.put(6, "6.png");
        boardConfig.imageAddresses.put(7, "7.png");
        boardConfig.imageAddresses.put(8, "8.png");


        boardConfig.initialOrdering.add(0);
        boardConfig.initialOrdering.add(5);
        boardConfig.initialOrdering.add(6);
        boardConfig.initialOrdering.add(7);
        boardConfig.initialOrdering.add(4);
        boardConfig.initialOrdering.add(3);
        boardConfig.initialOrdering.add(2);
        boardConfig.initialOrdering.add(8);
        boardConfig.initialOrdering.add(1);

















        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try (FileWriter writer = new FileWriter("src/ConfigFiles/Config.json")) {
            gson.toJson(boardConfig, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

     */

    public static BoardConfig getConfigInformation(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (Reader reader = new FileReader(
                "src/ConfigFiles/Config.json")) {

            BoardConfig boardConfig = gson.fromJson(reader, BoardConfig.class);
            return boardConfig;


        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }
    }

    public boolean buildTable(int panelWidth, int panelHeight,MainFrame mainFrame){

        String currentImageAddress;
        ArrayList<PuzzlePiece> puzzlePieces = new ArrayList<>();

        ArrayList<Integer> piecesRandomOrder = this.initialOrdering;
        Board.setMissingPiece(7);

        if (!Solvable.solvable(Board.getMissingPiece(), piecesRandomOrder)){
            FinishStates.setGameFinished(true);
            return false;
        }

        for (int i = 0; i < 9; i++) {
            if (Board.getMissingPiece() != i) {
                currentImageAddress = this.imageAddresses.get(piecesRandomOrder.get(i) + 1);

                puzzlePieces.add(new PuzzlePiece(currentImageAddress,
                        new Location(panelHeight / 3 * (i % 3), panelWidth / 3 * (i / 3))));
            } else {
                puzzlePieces.add(new PuzzlePiece("missing.jpg", new Location(
                        panelHeight / 3 * (i % 3), panelWidth / 3 * (i / 3))));
            }
        }
        Board.setPuzzlePieces(puzzlePieces);
        return true;
    }
}
