package SwingGUI.collectionPanel;

import SwingGUI.Drawer;
import Util.Constants;
import Util.ImageLoading;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class CollectionDeckPanel extends JPanel {

    private BufferedImage backgroundImage;
    private Drawer drawer;
    private static String Message = "";

    public static DeckManagementPanel deckManagementPanel;
    public static DeckCardPanel deckCardPanel;



    public CollectionDeckPanel() {
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.FILTER_PANEL_BACKGROUND_IMAGE_ADDRESS);

        drawer = new Drawer();
        BorderLayout layout = new BorderLayout();
        setLayout(layout);

        deckCardPanel = new DeckCardPanel();
        deckCardPanel.setPreferredSize(
                new Dimension(Constants.COLLECTION_CARD_PANEL_WIDTH, Constants.COLLECTION_CARD_PANEL_HEIGHT));

        deckCardPanel.setBorder(new LineBorder(Color.BLACK));
        JScrollPane scrollPane1 = new JScrollPane(deckCardPanel);
        scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        scrollPane1.setPreferredSize(new Dimension(1000, 500));
        scrollPane1.setLocation(200, 200);
        scrollPane1.setLayout(new ScrollPaneLayout());
        scrollPane1.setEnabled(true);
        add(scrollPane1, BorderLayout.EAST);


        deckManagementPanel = new DeckManagementPanel();
        deckManagementPanel.setPreferredSize(new Dimension(300, 800));
        deckManagementPanel.setBorder(new LineBorder(Color.BLACK));
        deckManagementPanel.setEnabled(true);
        add(deckManagementPanel, BorderLayout.WEST);
    }
    @Override
    protected void paintComponent(Graphics g){

        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.FRAME_WIDTH,
                Constants.FRAME_HEIGHT, null);




    }

    public static String getMessage() {
        return Message;
    }

    public static void setMessage(String message) {
        Message = message;
    }
}
