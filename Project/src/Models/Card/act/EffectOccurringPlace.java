package Models.Card.act;

public enum EffectOccurringPlace {

    FriendlyMinions, EnemyMinions,AllMinions, FriendlyHero, EnemyHero, AllHero, AllCharacters, AllFriendly, AllEnemy,
    RandomMinion, RandomHero, RandomCharacter, AllFriendlyHand, AllFriendlyDeck, AllFriendlyGround, AllEnemyHand, AllEnemyDeck,
    AllEnemyGround, AllHands,AllDecks,AllGrounds, ChooseMinion, ChooseHero, ChooseCharacter, ChooseInHand, ChooseInDeck, ChooseInGround,
    SummonedMinion



}
