package Interfaces;
import Models.Card.Card;
import Models.Card.Deck;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public interface DeckProducible {

    static Deck produceDeckObject(String deckName, ArrayList<String> deckCarts){

        // Producing A New Deck
        Deck deck = new Deck();
        // Setting A Name To Deck
        deck.setName(deckName);
        // Setting Cards To Deck
        deck.setCards(deckCarts);


        return deck;
    }
}
