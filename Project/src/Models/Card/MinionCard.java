package Models.Card;

import Interfaces.Visitable;
import Logic.PlayActions.DoPlay;
import Models.Hero.Hero;

public class MinionCard implements Visitable {
    private int Health;
    private int Attack;

    public MinionCard(int Health, int attack) {
        this.Health = Health;
        Attack = attack;
    }



    public int getHealth() {
        return Health;
    }

    public void setHealth(int health) {
        Health = health;
    }

    public int getAttack() {
        return Attack;
    }

    public void setAttack(int attack) {
        Attack = attack;
    }

    @Override
    public void accept(DoPlay doPlay) {
        doPlay.play(this);
    }


}
