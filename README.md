﻿## Advanced Programming

 - **HW1:** Sevaral implemenntations of different functions using sorting methods and several other methods, part 2 is implementing **snake and ladder game**
 - **HW2:** Implementing **Tetris game** using graphics
 - **HW3:** In this homework the code of a **puzzle-8 game** was given and we should have changed the code structure in order to satisfy the clean code conditions.
 - **HW4:** The aim of this homework was to implement a message passing system using multi-threaded message broker with several producers and consumers which can convey messages simultaneously.
 - **Project:** The aim of the project was to implement **the Hearthstone game** with desired characteristics such as graphics, different sections using files in order to save the game status, playing section which contains several actions and proper logging conditions. 


