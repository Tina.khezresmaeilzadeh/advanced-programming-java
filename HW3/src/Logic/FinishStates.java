package Logic;

import Util.Board;

public class FinishStates {
    private static String gameState = "#";
    private static boolean gameFinished = false;


    public static boolean gameFinished() {
        for (int i = 0; i < 9; i++) {
            int pieceIdentifier = Board.getPuzzlePieces().get(i).getPieceNumber();
            if (pieceIdentifier == 8) {
                continue;
            }

            if (pieceIdentifier != i) {
                return false;
            }
        }
        return true;
    }


    public static String getGameState() {
        return gameState;
    }

    public static void setGameState(String gameState) {
        FinishStates.gameState = gameState;
    }

    public static boolean isGameFinished() {
        return gameFinished;
    }

    public static void setGameFinished(boolean gameFinished) {
        FinishStates.gameFinished = gameFinished;
    }
}
