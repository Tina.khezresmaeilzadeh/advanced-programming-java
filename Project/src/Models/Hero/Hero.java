package Models.Hero;
import Models.Card.Card;

import java.util.ArrayList;

public class Hero  {

    protected String  NameOfHero;
    protected ArrayList<String> decksOfHeroNames;
    protected ArrayList<String> decksOfHero;
    int HPHero = 30;
    protected Card currentWeapon;
    protected boolean withWeapon = false;


    public void attack(Hero hero){

    }
    public void attack(Card card){

    }



    public int getHPHero() {
        return HPHero;
    }

    public void setHPHero(int HPHero) {
        this.HPHero = HPHero;
    }

    public Card getCurrentWeapon() {
        return currentWeapon;
    }

    public void setCurrentWeapon(Card currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    public boolean isWithWeapon() {
        return withWeapon;
    }

    public void setWithWeapon(boolean withWeapon) {
        this.withWeapon = withWeapon;
    }


}

