package Interfaces;

import Util.Board;
import Util.MyPanel;

public interface Movable extends Swappable{
    static void moveByKeys(String direction){

        int missingPieceIndex = Board.getMissingPiece();

        switch(direction){
            case "right" :
                if (missingPieceIndex % 3 == 2) {
                    return;
                }
                Swappable.swapPieces(missingPieceIndex, missingPieceIndex + 1);
                Board.setMissingPiece(missingPieceIndex + 1);
                break;
            case "left"  :

                if (missingPieceIndex % 3 == 0) {
                    return;
                }
                Swappable.swapPieces(missingPieceIndex, missingPieceIndex - 1);
                Board.setMissingPiece(missingPieceIndex - 1);
                break;

            case "up"  :

                if (missingPieceIndex <= 2) {
                    return;
                }
                Swappable.swapPieces(missingPieceIndex, missingPieceIndex - 3);
                Board.setMissingPiece(missingPieceIndex - 3);
                break;

            case "down"  :

                if (missingPieceIndex >= 6) {
                    return;
                }
                Swappable.swapPieces(missingPieceIndex, missingPieceIndex + 3);
                Board.setMissingPiece(missingPieceIndex + 3);
                break;
        }
    }
}

