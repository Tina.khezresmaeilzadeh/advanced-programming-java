package SwingGUI.shopPanel;
import SwingGUI.Drawer;
import Util.Constants;
import Util.ImageLoading;
import Util.MapperShop;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;


public class ShopPanel extends JPanel {

    private BufferedImage backgroundImage;
    private Constants constants;
    Drawer drawer ;

    public ShopManagementPanel shopManagementPanel;
    public static ShopCardPanel shopCardPanel;

    // States
    private static boolean buyState = true;
    private static boolean sellState = false;

    //Mapper Shop
    private MapperShop mapperShop = new MapperShop();

    private static HashMap<String, Rectangle> cardsNamesAndPositions = new HashMap<>();

    public static HashMap<String, Rectangle> getCardsNamesAndPositions() {
        return cardsNamesAndPositions;
    }

    public static void setCardsNamesAndPositions(HashMap<String, Rectangle> cardsNamesAndPositions) {
        ShopPanel.cardsNamesAndPositions = cardsNamesAndPositions;
    }

    public static boolean isBuyState() {
        return buyState;
    }

    public static void setBuyState(boolean buyState) {
        ShopPanel.buyState = buyState;
    }

    public static boolean isSellState() {
        return sellState;
    }

    public static void setSellState(boolean sellState) {
        ShopPanel.sellState = sellState;
    }

    public ShopPanel() {

        constants = new Constants();
        ImageLoading imageLoading = new ImageLoading();

        backgroundImage = imageLoading.loadImage(
                constants.SHOP_PANEL_BACKGROUND_IMAGE_ADDRESS);


        drawer = new Drawer();



        BorderLayout layout = new BorderLayout();
        setLayout(layout);





        shopCardPanel = new ShopCardPanel();
        shopCardPanel.setPreferredSize(
                new Dimension(Constants.COLLECTION_CARD_PANEL_WIDTH, Constants.COLLECTION_CARD_PANEL_HEIGHT));

        shopCardPanel.setBorder(new LineBorder(Color.BLACK));
        JScrollPane scrollPane1 = new JScrollPane(shopCardPanel);
        scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        scrollPane1.setPreferredSize(new Dimension(1000, 500));
        scrollPane1.setLocation(200, 200);
        scrollPane1.setLayout(new ScrollPaneLayout());
        scrollPane1.setEnabled(true);
        add(scrollPane1, BorderLayout.EAST);


        shopManagementPanel = new ShopManagementPanel();
        shopManagementPanel.setPreferredSize(new Dimension(300, 800));
        shopManagementPanel.setBorder(new LineBorder(Color.BLACK));
        shopManagementPanel.setEnabled(true);
        add(shopManagementPanel, BorderLayout.WEST);


    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, constants.FRAME_WIDTH,
                constants.FRAME_HEIGHT, null);











    }

}
