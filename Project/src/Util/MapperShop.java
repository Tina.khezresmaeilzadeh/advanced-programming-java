package Util;
import Logic.CollectionLogic;
import Logic.GameState;
import Models.Player;
import SwingGUI.shopPanel.ShopPanel;

import java.io.IOException;
import java.util.ArrayList;

public class MapperShop {
    private static ArrayList<String> currentCards = new ArrayList<>();


    public static void produceCurrentCards(boolean buyState, boolean sellState){
        Player player = GameState.getInstance().getPlayer();

        if(buyState){
            currentCards = new ArrayList<>();
            for(String cardName : CollectionLogic.readTotalCardNames())
                if(!player.getAvailableCards().contains(cardName)){
                    currentCards.add(cardName);
                }

        }else if(sellState){

            currentCards = player.getAvailableCards();



        }

    }
    public static void readyShop(){


        produceCurrentCards(ShopPanel.isBuyState(), ShopPanel.isSellState());

    }

    public static ArrayList<String> getCurrentCards() {
        return currentCards;
    }

    public static void setCurrentCards(ArrayList<String> currentCards) {
        MapperShop.currentCards = currentCards;
    }
}
