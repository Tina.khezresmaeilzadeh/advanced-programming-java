package SwingGUI.statusPanel;
import Logic.StatusLogic;
import SwingGUI.shopPanel.ShopCardPanel;
import Util.Constants;
import Util.ImageLoading;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.image.BufferedImage;


public class StatusPanel extends JPanel {

    private Constants constants;
    private static StatusCardPanel statusCardPanel;

    public StatusPanel() {

        constants = new Constants();
        BorderLayout layout = new BorderLayout();
        setLayout(layout);

        statusCardPanel = new StatusCardPanel();
        statusCardPanel.setPreferredSize(
                new Dimension(Constants.STATUS_CARD_PANEL_WIDTH, Constants.STATUS_CARD_PANEL_HEIGHT));

        statusCardPanel.setBorder(new LineBorder(Color.BLACK));
        JScrollPane scrollPane1 = new JScrollPane(statusCardPanel);
        scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        scrollPane1.setPreferredSize(new Dimension(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT));
        scrollPane1.setLocation(200, 200);
        scrollPane1.setLayout(new ScrollPaneLayout());
        scrollPane1.setEnabled(true);
        add(scrollPane1, BorderLayout.EAST);

    }


}
