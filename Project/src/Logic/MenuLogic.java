package Logic;
import Models.Player;
import SwingGUI.MainFrame;
import Util.Log;
import Util.MapperShop;

import java.io.IOException;

public class MenuLogic {
    private Player player;

    public MenuLogic() {

    }

    public void playButtonClicked() throws IOException {



        player = GameState.getInstance().getPlayer();

        if( player.isHasChosenDeck() && player.isHasChosenDeck()){

            MainFrame.showInfoPassivePanel();

        }else
            collectionButtonClicked();

        Log.writeBodyInLogFile("CLICKED", "PlayButton");


    }

    public void collectionButtonClicked() throws IOException {



        player = GameState.getInstance().getPlayer();
        Log.writeBodyInLogFile("CLICKED", "CollectionButton");
        MainFrame.showCollectionCardManagementPanel();

    }

    public void statusButtonClicked() throws IOException {

        player = GameState.getInstance().getPlayer();
        Log.writeBodyInLogFile("CLICKED", "StatusButton");
        MainFrame.showStatusPanel();

    }

    public void shopButtonClicked() throws IOException {

        player = GameState.getInstance().getPlayer();
        MapperShop.readyShop();
        Log.writeBodyInLogFile("CLICKED", "ShopButton");
        MainFrame.showShopPanel();

    }

    public void settingButtonClicked() throws IOException {


        player = GameState.getInstance().getPlayer();
        Log.writeBodyInLogFile("CLICKED", "PlayButton");



    }

}
