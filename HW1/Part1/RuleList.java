import javax.xml.stream.events.Characters;
import java.util.*;
import java.lang.*;
import java.io.*;

public class RuleList {
	//IN THIS CLASS, YOU NEED TO DEFINE THE APPROPRIATE FIELD(S)
	List<String> lists = new ArrayList<String>();

	/**
	 * Constructs a new Taboo using the given rules
	 *
	 * @param rules rules for new Taboo
	 */
	public RuleList(List<String> rules) {
		lists = rules;


	}

	/**
	 * Returns the set of elements which should not follow
	 * the given element.
	 *
	 * @param elem
	 * @return elements which should not follow the given element
	 */
	public Set<String> noFollow(String elem) {
		Set<String> Output = new HashSet<String>();
		int counter = 0;
		for (int i = 0; i < lists.size() - 1; i++) {
			if (lists.get(i) == elem) {
				Output.add(lists.get(i + 1));
				counter++;


			}

		}
		if (counter == 0) {
			return Collections.EMPTY_SET;
		} else
			return Output; // YOUR CODE HERE
	}

	/**
	 * Removes elements from the given list that
	 * violate the rules
	 *
	 * @param list collection to reduce
	 */

	public void reduce(List<String> list) {

		for(int i =0 ;i<list.size()-1;i++)
		{

			while(i<list.size()-1 && noFollow(list.get(i)).contains(list.get(i+1)) )
			{
				int j = i +1;
				do
				{
					j = i +1;

					while( j<=list.size()-2 && noFollow(list.get(j)).contains(list.get(j+1)) )
					{
						j++;

					}
					list.remove(j);



				}while (j != i+1);



			}

		}
		System.out.println(list);
	}









}
