import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class Canvas extends JPanel implements ActionListener {


    private Vector2D currentPosition = new Vector2D(0, 0);

    private JLabel statusBar1;
    private JLabel statusBar2;
    private JLabel statusBar3;

    private Shape currentShape;
    private Shape lastShape;
    private static Shape nextShape;

    private Constants.DefaultShapes[] board;
    private Constants.DefaultShapes[] lastPart;
    private int score = 0;
    private int scoreOfEachStage = 0;
    private ArrayList<Integer> scores = new ArrayList<Integer>();
    private boolean firstTime = true;
    private boolean undoForFirstTime = true;
    private Timer timer;
    private boolean readyForNextStage = false;
    private boolean isStarted = false;
    private boolean isPaused = false;
    private boolean isUndo = false;
    private int numLinesRemoved = 0;



    public Canvas(Window parent) {
        setFocusable(true);
        currentShape = new Shape();
        lastShape = new Shape();
        nextShape = new Shape();
        timer = new Timer(400, this); // timer for lines down
        statusBar1 = parent.getStatusBar1();
        statusBar2 = parent.getStatusBar2();
        statusBar3 = parent.getStatusBar3();
        board = new Constants.DefaultShapes[Constants.BOARD_WIDTH * Constants.BOARD_HEIGHT];
        lastPart = new Constants.DefaultShapes[Constants.BOARD_WIDTH * Constants.BOARD_HEIGHT];
        clearBoard();
        addKeyListener(new MyTetrisAdapter());
    }


    public Shape getNextShape() {
        return nextShape;
    }

    public void setNextShape(Shape nextShape) {
        this.nextShape = nextShape;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (readyForNextStage) {
            readyForNextStage = false;
            newPiece();
            undoForFirstTime = false;
        } else {
            oneLineDown();
        }
    }



    public Constants.DefaultShapes shapeByGivingIndex(int x, int y) {
        return board[y * Constants.BOARD_WIDTH + x];
    }

    private void clearBoard() {
        for (int i = 0; i < Constants.BOARD_HEIGHT * Constants.BOARD_WIDTH; i++) {
            board[i] = Constants.DefaultShapes.BiShekl;
            lastPart[i] = Constants.DefaultShapes.BiShekl;
        }
    }

    private int maxOfScores(){
        int max = 0;
        for(Integer x : scores){

            if(x > max)
                max = x;

        }
        return max;
    }





    public void newPiece() {

        //scores part
        scoreOfEachStage += 1;
        score += 1;

        if(firstTime){
            scoreOfEachStage--;
            score--;
        }


        scores.add(scoreOfEachStage);

        statusBar1.setText("score: " + score + " max score: "+ maxOfScores() );
       // statusBar2.setText("max score: "+ maxOfScores());

        scoreOfEachStage = 0;
        // undo actions
        for (int i=0; i<board.length; i++)
            lastPart[i] = board[i];


        for (int i = 0; i < 4; i++) {
            int x = currentPosition.getX() + currentShape.getArr2d()[i][0];
            int y = currentPosition.getY() - currentShape.getArr2d()[i][1];
            lastPart[y * Constants.BOARD_WIDTH+ x] = Constants.DefaultShapes.BiShekl;
        }


        if(isUndo){
            currentShape.setShape(lastShape.getShape());

        }else{
            lastShape.setShape(currentShape.getShape());
            if(firstTime){
                currentShape.setRandomShape();
                nextShape.setRandomShape();
                statusBar2.setText("Number of removed lines: "+ numLinesRemoved + "  next shape: "+ nextShape.getPieceShape().name);
            }else{
                currentShape.setShape(nextShape.getShape());
                nextShape.setRandomShape();
                statusBar2.setText("Number of removed lines: "+ numLinesRemoved + "  next shape: "+ nextShape.getPieceShape().name);
            }



        }



        isUndo = false;





        currentPosition.setX( Constants.BOARD_WIDTH / 2 + 1 );
        currentPosition.setY( Constants.BOARD_HEIGHT - 1 + currentShape.minY() );


        if (!checkMovementCorrect(currentShape, currentPosition.getX(), currentPosition.getY() - 1)) {
            currentShape.setShape(Constants.DefaultShapes.BiShekl);
            timer.stop();
            isStarted = false;
            clearBoard();
            repaint();
            statusBar2.setText("Game Over :((((");
            statusBar1.setText("Wish you luck in next turns !");
        }
    }








    private void drawSquare(Graphics g, int x, int y, Constants.DefaultShapes shape) {

        g.setColor(shape.getColor());
        g.fillRect(x + 1, y + 1, (int) getSize().getWidth() / Constants.BOARD_WIDTH - 2, (int) getSize().getHeight() / Constants.BOARD_HEIGHT - 2);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Dimension size = getSize();
        int boardTop = (int) size.getHeight() - Constants.BOARD_HEIGHT * (int) getSize().getHeight() / Constants.BOARD_HEIGHT;

        for (int i = 0; i < Constants.BOARD_HEIGHT; i++) {
            for (int j = 0; j < Constants.BOARD_WIDTH; ++j) {
                Constants.DefaultShapes shape = shapeByGivingIndex(j, Constants.BOARD_HEIGHT - i - 1);

                if (shape != Constants.DefaultShapes.BiShekl) {

                    drawSquare(g, j * (int) getSize().getWidth() / Constants.BOARD_WIDTH, boardTop + i * (int) getSize().getHeight() / Constants.BOARD_HEIGHT, shape);
                }
            }
        }

        if (currentShape.getShape() != Constants.DefaultShapes.BiShekl) {
            for (int i = 0; i < 4; ++i) {
                int x = currentPosition.getX() + currentShape.getArr2d()[i][0];
                int y = currentPosition.getY() - currentShape.getArr2d()[i][1];

                drawSquare(g, x * (int) getSize().getWidth() / Constants.BOARD_WIDTH, boardTop + (Constants.BOARD_HEIGHT - y - 1) * (int) getSize().getHeight() / Constants.BOARD_HEIGHT, currentShape.getShape());
            }
        }
    }



    public void start() {
        if (isPaused)
            return;

        isStarted = true;
        readyForNextStage = false;
        numLinesRemoved = 0;
        clearBoard();
        newPiece();
        timer.start();
    }






    private boolean checkMovementCorrect(Shape newPiece, int newX, int newY) {
        firstTime = false;

        for (int i = 0; i < 4; ++i) {
            int x = newX + newPiece.getArr2d()[i][0];
            int y = newY - newPiece.getArr2d()[i][1];

            if (x < 0 || x >= Constants.BOARD_WIDTH || y < 0 || y >= Constants.BOARD_HEIGHT)
                return false;

            if (shapeByGivingIndex(x, y) != Constants.DefaultShapes.BiShekl)
                return false;
        }

        currentShape = newPiece;
        currentPosition.setX(newX);
        currentPosition.setY(newY);

        repaint();

        return true;
    }

    private void oneLineDown() {
        if (!checkMovementCorrect(currentShape, currentPosition.getX(), currentPosition.getY() - 1))
            dropOrOneLineDown();
    }

    private void dropOrOneLineDown() {


        for (int i = 0; i < 4; i++) {
            int x = currentPosition.getX() + currentShape.getArr2d()[i][0];
            int y = currentPosition.getY() - currentShape.getArr2d()[i][1];
            board[y * Constants.BOARD_WIDTH+ x] = currentShape.getShape();
        }

        removeFullLines();



        if (!readyForNextStage) {

            //System.out.println(scoreOfEachStage);

            newPiece();
            undoForFirstTime = false;
        }


    }



    private void removeFullLines() {
        int numFullLines = 0;

        for (int i = Constants.BOARD_HEIGHT - 1; i >= 0; --i) {
            boolean lineIsFull = true;

            for (int j = 0; j < Constants.BOARD_WIDTH; ++j) {
                if (shapeByGivingIndex(j, i) == Constants.DefaultShapes.BiShekl) {
                    lineIsFull = false;
                    break;
                }
            }

            if (lineIsFull) {
                ++numFullLines;
                score +=10;
                scoreOfEachStage += 10;



                for (int k = i; k < Constants.BOARD_HEIGHT - 1; ++k) {
                    for (int j = 0; j < Constants.BOARD_WIDTH; ++j) {
                        board[k * Constants.BOARD_WIDTH + j] = shapeByGivingIndex(j, k + 1);
                    }
                }
            }

            if (numFullLines > 0) {
                numLinesRemoved += numFullLines;
                statusBar2.setText("Number of removed lines: "+ numLinesRemoved + "  next shape: "+ nextShape.getPieceShape().name);

                readyForNextStage = true;
                currentShape.setShape(Constants.DefaultShapes.BiShekl);
                repaint();
            }
        }
    }

    private void undo(){
        if(!firstTime){
            for (int i=0; i<lastPart.length; i++)
                board[i] = lastPart[i];

            isUndo = true;
            newPiece();
        }





    }





    class MyTetrisAdapter implements KeyListener {
        @Override
        public void keyTyped(KeyEvent keyEvent) {

        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {
            if (!isStarted || currentShape.getShape() == Constants.DefaultShapes.BiShekl)
                return;

            switch (keyEvent.getKeyCode()) {
                case KeyEvent.VK_LEFT:
                    checkMovementCorrect(currentShape, currentPosition.getX() - 1, currentPosition.getY());
                    break;
                case KeyEvent.VK_RIGHT:
                    checkMovementCorrect(currentShape, currentPosition.getX() + 1, currentPosition.getY());
                    break;
                case KeyEvent.VK_DOWN:
                    checkMovementCorrect(currentShape.rotateClockwise(), currentPosition.getX(), currentPosition.getY());
                    break;
                case KeyEvent.VK_UP:
                    checkMovementCorrect(currentShape.rotateAntiClockwise(), currentPosition.getX(), currentPosition.getY());
                    break;
                case 'u':
                case 'U' :

                    if(!undoForFirstTime)
                        undo();
                    break;



            }

        }

        @Override
        public void keyReleased(KeyEvent keyEvent) {

        }
    }

}
