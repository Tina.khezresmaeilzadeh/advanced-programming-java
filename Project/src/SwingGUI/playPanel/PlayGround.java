package SwingGUI.playPanel;

import Logic.GameState;
import Logic.PlayLogic;
import SwingGUI.Drawer;
import SwingGUI.collectionPanel.CollectionCardManagement;
import SwingGUI.collectionPanel.CollectionDeckPanel;
import Util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

public class PlayGround extends JPanel implements MouseListener, MouseMotionListener {

    private BufferedImage backgroundImage;
    private BufferedImage friendlyHero;
    private BufferedImage opponentHero;
    private Constants constants;
    private Rectangle endTurn ;
    private Drawer drawer;

    public PlayGround() {

        constants = new Constants();
        drawer = new Drawer();
        ImageLoading imageLoading = new ImageLoading();
        endTurn = new Rectangle(1170, 335, 140, 70);
        backgroundImage = imageLoading.loadImage(
                constants.PLAYGROUND_BACKGROUND_IMAGE_ADDRESS);
        friendlyHero = imageLoading.loadImage(
                "src/Photos/PlayGroundHero/Priest.png");
        addMouseListener(this);
        setLayout(new BorderLayout());
        start();

    }

    private void start() {
        javax.swing.Timer t = new javax.swing.Timer(16, new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                draw();
            }
        });
        t.start();
    }

    private void draw() {
        this.repaint();
        this.revalidate();
    }


    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, constants.FRAME_WIDTH,
                constants.FRAME_HEIGHT, null);

        g2d.drawImage(this.friendlyHero,
                (int) 615,
                (int) 50, 200,
                200, null);

        g2d.drawImage(this.friendlyHero,
                (int) 615,
                (int) 510, 200,
                200, null);


        drawer.drawPlayGround(g2d);
        drawer.drawFriendlyHandCards(g2d, GameState.getInstance().getPlayer().getCurrentHandCards());
        drawer.drawFriendlyOnGroundCards(g2d, GameState.getInstance().getPlayer().getCurrentOnGroundCards());
        drawer.drawEnemyHandCards(g2d, GameState.getInstance().getOpponentPlayer().getCurrentHandCards());
        drawer.drawEnemyOnGroundCards(g2d, GameState.getInstance().getOpponentPlayer().getCurrentOnGroundCards());







    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int x = mouseEvent.getX();
        int y = mouseEvent.getY();
        if(endTurn.contains(x,y)){

            PlayLogic.endTheTurn();


        }else{

            if(GameState.getInstance().isFriendlyTurn()){
                System.out.println("we are clicking in a friendly turn");
                for(Rectangle rectangle: MapperPlay.getCurrentFriendlyOnHandRectangles()){

                    if(rectangle.contains(x, y)){

                        int index = MapperPlay.getCurrentFriendlyOnHandRectangles().indexOf(rectangle);
                        String cardNameSelected = MapperPlay.getCurrentFriendlyOnHandCards().get(index);
                        PlayLogic.putCardOnGround(cardNameSelected, GameState.getInstance().getPlayer());
                        PlayLogic.summonCard(rectangle.x, rectangle.y);
                        this.repaint();


                    }
                }
            }else{
                System.out.println("we are clicking in an enemy turn");
                for(Rectangle rectangle: MapperPlay.getCurrentEnemyOnHandRectangles()){

                    if(rectangle.contains(x, y)){

                        int index = MapperPlay.getCurrentEnemyOnHandRectangles().indexOf(rectangle);
                        String cardNameSelected = MapperPlay.getCurrentEnemyOnHandCards().get(index);
                        PlayLogic.putCardOnGround(cardNameSelected, GameState.getInstance().getOpponentPlayer());
                        PlayLogic.summonCard(rectangle.x, rectangle.y);
                        this.repaint();


                    }
                }
            }


        }
        this.repaint();

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }
}
