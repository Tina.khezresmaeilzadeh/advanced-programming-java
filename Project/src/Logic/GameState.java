package Logic;

import Models.Hero.Hero;
import Models.Player;

import java.util.ArrayList;

public class GameState {

    private static GameState currentGameState = null;
    private Player player = new Player();
    private Player opponentPlayer = new Player();
    private Player currentPlayer = player;

    private boolean friendlyTurn = true;


    private Hero friendlyHero = new Hero();
    private Hero enemyHero = new Hero();




    // private constructor restricted to this class itself
    private GameState() {

    }

    // static method to create instance of GameState class
    public static GameState getInstance() {
        if (currentGameState == null)
            currentGameState = new GameState();

        return currentGameState;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Hero getFriendlyHero() {
        return friendlyHero;
    }

    public void setFriendlyHero(Hero friendlyHero) {
        this.friendlyHero = friendlyHero;
    }

    public Hero getEnemyHero() {
        return enemyHero;
    }

    public void setEnemyHero(Hero enemyHero) {
        this.enemyHero = enemyHero;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public boolean isFriendlyTurn() {
        return friendlyTurn;
    }

    public void setFriendlyTurn(boolean friendlyTurn) {
        this.friendlyTurn = friendlyTurn;
    }

    public Player getOpponentPlayer() {
        return opponentPlayer;
    }

    public void setOpponentPlayer(Player opponentPlayer) {
        this.opponentPlayer = opponentPlayer;
    }
}