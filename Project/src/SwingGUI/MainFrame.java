package SwingGUI;

import SwingGUI.collectionPanel.CollectionCardManagement;
import SwingGUI.collectionPanel.CollectionDeckPanel;
import SwingGUI.playPanel.InfoPassivePanel;
import SwingGUI.playPanel.PlayGround;
import SwingGUI.shopPanel.ShopPanel;
import SwingGUI.statusPanel.StatusPanel;
import Util.Constants;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private static JPanel panelContent = new JPanel();
    private static CardLayout card = new CardLayout();
    private LogInPanel logInPanel = new LogInPanel();
    private MenuPanel menuPanel = new MenuPanel();
    private PlayGround playGround = new PlayGround();
    private ShopPanel shopPanel = new ShopPanel();
    private StatusPanel statusPanel = new StatusPanel();
    private CollectionCardManagement collectionCardManagement = new CollectionCardManagement();
    private CollectionDeckPanel collectionDeckPanel = new CollectionDeckPanel();
    private InfoPassivePanel infoPassivePanel = new InfoPassivePanel();

    public MainFrame() {

        /*
        super("Hello World ");
        setLayout(new BorderLayout());
        this.setContentPane(this.logInPanel);
        setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);

         */

        super("ProjectileGame");
        add(panelContent);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);

        panelContent.setLayout(card);
        panelContent.add(logInPanel, "1");
        panelContent.add(menuPanel, "2");
        panelContent.add(playGround, "3");
        panelContent.add(shopPanel, "4");
        panelContent.add(statusPanel, "5");
        panelContent.add(collectionCardManagement, "6");
        panelContent.add(collectionDeckPanel, "7");
        panelContent.add(infoPassivePanel, "8");


        card.show(panelContent, "1");   //First JPanel to be shown is that of the Log-in Panel.

        setVisible(true);


    }
    public static void showLogInPanel() {
        card.show(panelContent, "1");
    }

    public static void showMenuPanel() {
        card.show(panelContent, "2");
    }

    public static void showPlayGround(){
        card.show(panelContent, "3");
    }



    public static void showShopPanel(){
        card.show(panelContent, "4");
    }

    public static void showStatusPanel(){
        card.show(panelContent, "5");
    }

    public static void showCollectionCardManagementPanel(){
        card.show(panelContent, "6");
    }

    public static void showCollectionDeckPanel(){
        card.show(panelContent, "7");
    }

    public static void showInfoPassivePanel(){
        card.show(panelContent, "8");
    }

}
