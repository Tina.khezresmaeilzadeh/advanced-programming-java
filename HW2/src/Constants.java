import java.awt.*;
import java.util.Random;

public class Constants {
    public static final int BOARD_WIDTH = 10;
    public static final int BOARD_HEIGHT = 22;
    public static final int NUMBER_OF_SHAPES = 7;
    public static final int DELAY_TIME = 1000;
    public static final int PERIOD_TIME = 16;
    public static final int FRAME_WIDTH = 300;
    public static final int FRAME_HEIGHT = 600;


    public enum DefaultShapes {
        BiShekl(new int[][] { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } }, new Color(0, 0, 0)  ,"Bishekl"),
        RightDeck(new int[][] { { 0, -1 }, { 0, 0 }, { -1, 0 }, { -1, 1 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)), "RightDeck"),
        LeftDeck(new int[][] { { 0, -1 }, { 0, 0 }, { 1, 0 }, { 1, 1 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)), "LeftDeck"),
        Wood(new int[][] { { 0, -1 }, { 0, 0 }, { 0, 1 }, { 0, 2 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)) , "Wood"),
        Mountain(new int[][] { { -1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 1 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)), "Mountain"),
        window(new int[][] { { 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)), "window"),
        LeftLeg(new int[][] { { -1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)), "leftleg"),
        RightLeg(new int[][] { { 1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 } }, new Color(new Random().nextInt(256) , new Random().nextInt(256), new Random().nextInt(256)), "rightleg");

        private int[][] coordinates;
        private Color color;
        String name;

        public int[][] getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(int[][] coordinates) {
            this.coordinates = coordinates;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;

        }

        DefaultShapes(int[][] coordinates, Color c , String name) {
            this.coordinates = coordinates;
            color = c;
            this.name = name;
        }

    }


}
