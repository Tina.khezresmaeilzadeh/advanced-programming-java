package GUI;
import Logic.FinishStates;
import Util.MyKeyListener;
import Util.MyPanel;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    public static MyPanel panel;

    public MainFrame() throws HeadlessException {

        super("Puzzle");
        panel = new MyPanel();
        add(panel);
        int screenWidth, screenHeight;
        screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        int maxSize = Math.max(screenWidth, screenHeight) / 3;
        panel.setSize(maxSize, maxSize);
        panel.setLocation(screenWidth / 2 - maxSize / 2, screenHeight / 2 - maxSize / 2);
        setSize(panel.getSize());
        setLocation(panel.getLocation());
        add(panel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new MyKeyListener());
        setVisible(true);




    }
    public void showProperErrors(boolean withoutErrors){

        if(!withoutErrors){

            JOptionPane.showMessageDialog(this,
                    "this puzzle is not solvable, change your config and try again",
                    "Puzzle not solvable", JOptionPane.WARNING_MESSAGE);
        }


    }


}
