package Models;
import Interfaces.DeckUpdatable;
import Models.Card.Deck;
import Models.Hero.Hero;

import java.util.ArrayList;
import java.util.HashMap;

public class Player implements DeckUpdatable {
    private String  username;
    private String password;
    private int numberOfManas = 50;
    private ArrayList<Deck>  allDecks = new ArrayList<>();
    private ArrayList<String> availableCards = new ArrayList<>();
    private Deck currentDeck ;
    private String currentHero;
    private ArrayList<String> allHeros;
    private boolean hasChosenHero = false;
    private boolean hasChosenDeck = false;
    private int currentTurnTotalManas = 1;
    private int currentTurnResumedManas = 1;
    private ArrayList<String> currentHandCards = new ArrayList<>();
    private ArrayList<String> currentOnGroundCards = new ArrayList<>();
    private String currentPassive = "";


    {
        availableCards.add("Polymorph");
        availableCards.add("Alexstrasza");
        availableCards.add("Blur");
        availableCards.add("Locust");
        availableCards.add("Moonfire");
        availableCards.add("BloodFury");
        availableCards.add("DraconicEmissary");
        availableCards.add("DragonClaw");
        availableCards.add("GuardBot");
        availableCards.add("SwampKingDred");


        // 1
        ArrayList<String> mageSpecialCards = new ArrayList<>();
        mageSpecialCards.add("Polymorph");
        mageSpecialCards.add("Alexstrasza");
        mageSpecialCards.add("Blur");
        mageSpecialCards.add("Blur");
        mageSpecialCards.add("Locust");
        mageSpecialCards.add("Polymorph");
        mageSpecialCards.add("Moonfire");
        mageSpecialCards.add("BloodFury");
        mageSpecialCards.add("Locust");

        Deck mageSpecial = new Deck("Mage Special", mageSpecialCards ,"Mage");

        // 2
        ArrayList<String> neutralHeavyDeckCards = new ArrayList<>();
        neutralHeavyDeckCards.add("DraconicEmissary");
        neutralHeavyDeckCards.add("DragonClaw");
        neutralHeavyDeckCards.add("DragonClaw");
        neutralHeavyDeckCards.add("Blur");
        neutralHeavyDeckCards.add("Locust");
        neutralHeavyDeckCards.add("GuardBot");
        neutralHeavyDeckCards.add("Moonfire");
        neutralHeavyDeckCards.add("BloodFury");
        neutralHeavyDeckCards.add("Locust");

        Deck neutralHeavyDeck = new Deck("Neutral Heavy", neutralHeavyDeckCards ,"Neutral");

        // 3
        ArrayList<String> hunterSpecialCards = new ArrayList<>();
        hunterSpecialCards.add("DraconicEmissary");
        hunterSpecialCards.add("SwampKingDred");
        hunterSpecialCards.add("Blur");
        hunterSpecialCards.add("Blur");
        hunterSpecialCards.add("BloodFury");
        hunterSpecialCards.add("GuardBot");
        hunterSpecialCards.add("Moonfire");
        hunterSpecialCards.add("BloodFury");
        hunterSpecialCards.add("Locust");

        Deck hunterSpecial = new Deck("Hunter Special", hunterSpecialCards ,"Hunter");


        currentDeck = mageSpecial;
        allDecks.add(mageSpecial);
        allDecks.add(neutralHeavyDeck);
        allDecks.add(hunterSpecial);

    }

    public Player() {
        for(Deck deck : this.getAllDecks()){
            DeckUpdatable.updateDeck(deck);
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNumberOfManas() {
        return numberOfManas;
    }

    public void setNumberOfManas(int numberOfManas) {
        this.numberOfManas = numberOfManas;
    }

    public ArrayList<Deck> getAllDecks() {
        return allDecks;
    }

    public void setAllDecks(ArrayList<Deck> allDecks) {
        this.allDecks = allDecks;
    }

    public Deck getCurrentDeck() {
        return currentDeck;
    }

    public void setCurrentDeck(Deck currentDeck) {
        this.currentDeck = currentDeck;
    }

    public String getCurrentHero() {
        return currentHero;
    }

    public void setCurrentHero(String currentHero) {
        this.currentHero = currentHero;
    }

    public ArrayList<String> getAllHeros() {
        return allHeros;
    }

    public void setAllHeros(ArrayList<String> allHeros) {
        this.allHeros = allHeros;
    }

    public boolean isHasChosenHero() {
        return hasChosenHero;
    }

    public void setHasChosenHero(boolean hasChosenHero) {
        this.hasChosenHero = hasChosenHero;
    }

    public boolean isHasChosenDeck() {
        return hasChosenDeck;
    }

    public void setHasChosenDeck(boolean hasChosenDeck) {
        this.hasChosenDeck = hasChosenDeck;
    }

    public ArrayList<String> getAvailableCards() {
        return availableCards;
    }

    public void setAvailableCards(ArrayList<String> availableCards) {
        this.availableCards = availableCards;
    }

    public int getCurrentTurnTotalManas() {
        return currentTurnTotalManas;
    }

    public void setCurrentTurnTotalManas(int currentTurnTotalManas) {
        this.currentTurnTotalManas = currentTurnTotalManas;
    }

    public int getCurrentTurnResumedManas() {
        return currentTurnResumedManas;
    }

    public void setCurrentTurnResumedManas(int currentTurnResumedManas) {
        this.currentTurnResumedManas = currentTurnResumedManas;
    }

    public ArrayList<String> getCurrentHandCards() {
        return currentHandCards;
    }

    public void setCurrentHandCards(ArrayList<String> currentHandCards) {
        this.currentHandCards = currentHandCards;
    }

    public ArrayList<String> getCurrentOnGroundCards() {
        return currentOnGroundCards;
    }

    public void setCurrentOnGroundCards(ArrayList<String> currentOnGroundCards) {
        this.currentOnGroundCards = currentOnGroundCards;
    }

    public String getCurrentPassive() {
        return currentPassive;
    }

    public void setCurrentPassive(String currentPassive) {
        this.currentPassive = currentPassive;
    }
}
