package SwingGUI.playPanel;

import Logic.PlayLogic;
import SwingGUI.Drawer;
import SwingGUI.MainFrame;
import SwingGUI.collectionPanel.CollectionCardManagement;
import SwingGUI.collectionPanel.CollectionDeckPanel;
import Util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class InfoPassivePanel extends JPanel implements MouseListener {

    private BufferedImage backgroundImage;
    private Drawer drawer;
    private String message = "";
    private MapperPassive mapperPassive = new MapperPassive();


    private Rectangle exit = new Rectangle(1190, 670, 140, 70 );
    private Rectangle menu = new Rectangle(30, 670, 140, 70 );
    private Rectangle startGame = new Rectangle(540, 550, 200, 70);


    public InfoPassivePanel() {

        addMouseListener(this);
        ImageLoading imageLoading = new ImageLoading();
        backgroundImage = imageLoading.loadImage(
                Constants.STATUS_CARD_PANEL_BACKGROUND_IMAGE_ADDRESS);
        drawer = new Drawer();

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;


        // Background
        g2d.drawImage(this.backgroundImage,
                (int) 0,
                (int) 0, Constants.STATUS_CARD_PANEL_WIDTH,
                Constants.STATUS_CARD_PANEL_HEIGHT, null);

        // Title
        g2d.setColor(Color.ORANGE);
        g2d.setFont(new Font("Courier New", Font.BOLD, 50));
        g2d.drawString("Info Passive", 500, 40);

        // Description
        g2d.setColor(Color.white);
        g2d.setFont(new Font("Courier New", Font.BOLD, 30));
        g2d.drawString("Please choose one of these passives.", 30, 100);


        // Draw Passives
        drawer.drawPassives(g2d, mapperPassive);

        //Buttons
        drawer.drawExitAndMenuButtons(g2d);

        // Draw Message
        g2d.setColor(Color.RED);
        g2d.setFont(new Font("Courier New", Font.BOLD, 30));
        g2d.drawString(getMessage(),  400, 400);






    }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        int x = mouseEvent.getX();
        int y = mouseEvent.getY();

        for(Rectangle rectangle: MapperPassive.getPassiveItemsRectangles()){

            if(rectangle.contains(x, y)){

                int index = MapperPassive.getPassiveItemsRectangles().indexOf(rectangle);
                String passiveNameSelected = MapperPassive.getPassiveItems().get(index);

                setMessage("Your Passive in this Game is : " + passiveNameSelected);


                MapperPassive.setPassiveNameSelected(passiveNameSelected);
                this.repaint();

            }
        }
        if(menu.contains(x, y)){
            try {
                Log.writeBodyInLogFile("CLICK", "menuButton");
            } catch (IOException e) {
                e.printStackTrace();
            }
            MainFrame.showMenuPanel();
        }else if(exit.contains(x, y)){
            try {

                Log.writeBodyInLogFile("exit", "From_Status");
                Log.saveThePlayer();
                System.exit(0);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(startGame.contains(x, y)){
            try {
                Log.writeBodyInLogFile("CLICK", "Start_Game");
            } catch (IOException e) {
                e.printStackTrace();
            }
            PlayLogic.prepareForTheGame();
            MainFrame.showPlayGround();
        }

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
